/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <stdlib.h>
#include <wealth.h>
#include <android/log.h>
#include "item.h"
#include "log.h"


JNIEXPORT jstring JNICALL itemDescription( JNIEnv* env, jclass cls, jlong itemHandle )
{
	const financial_item_t* item = (financial_item_t*)(intptr_t) itemHandle;
	const char* description = financial_item_description( item );
	return (*env)->NewStringUTF( env, description );
}

JNIEXPORT void JNICALL itemSetDescription( JNIEnv* env, jclass cls, jlong itemHandle, jstring desc )
{
	financial_item_t* item = (financial_item_t*)(intptr_t) itemHandle;
	const char *description = (*env)->GetStringUTFChars( env, desc, NULL );
	LOGI( "Setting description to \"%s\" in item %p\n", description, item );
	financial_item_set_description( item, description );
}

JNIEXPORT jdouble JNICALL itemAmount( JNIEnv* env, jclass cls, jlong itemHandle )
{
	const financial_item_t* item = (financial_item_t*)(intptr_t) itemHandle;
	return financial_item_amount( item );
}

JNIEXPORT void JNICALL itemSetAmount( JNIEnv* env, jclass cls, jlong itemHandle, jdouble amount )
{
	financial_item_t* item = (financial_item_t*)(intptr_t) itemHandle;
	LOGI( "Setting amount to $%.2f in item %p\n", amount, item );
	financial_item_set_amount( item, amount );
}
