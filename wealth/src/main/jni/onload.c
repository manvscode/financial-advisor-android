/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <jni.h>
#include <stdlib.h>
#include "libstate.h"
#include "log.h"
#include "calc.h"
#include "profile.h"
#include "item.h"
#include "asset.h"
#include "liability.h"

libstate_t library_state;
JavaVM* g_vm = NULL;

static JNINativeMethod CALC_CLASS_METHODS[] = {
    { "simpleInterest",     "(DDD)D", (void*) simpleInterest },
    { "compoundInterest",   "(DDD)D", (void*) compoundInterest },
    { "annuityPresentValue",   "(DDD)D", (void*) annuityPresentValue },
    { "annuityDuePresentValue",   "(DDD)D", (void*) annuityDuePresentValue },
    { "annuityFutureValue",   "(DDD)D", (void*) annuityFutureValue },
    { "annuityDueFutureValue",   "(DDD)D", (void*) annuityDueFutureValue },
};
static const char* CALC_CLASS_NAME = "com/manvscode/wealth/Calc";
static const size_t CALC_CLASS_METHOD_COUNT = sizeof(CALC_CLASS_METHODS) /  sizeof(CALC_CLASS_METHODS[0]);
// ------------------------------------------------
static JNINativeMethod PROFILE_CLASS_METHODS[] = {
    { "profileCreate",                "()J", (void*) profileCreate },
    { "profileDestroy",               "(J)V", (void*) profileDestroy },
    { "profileLoad",                  "(Ljava/lang/String;)J", (void*) profileLoad },
    { "profileSave",                  "(JLjava/lang/String;)Z", (void*) profileSave },
    { "profileSetUpdatedCallback",    "(JLcom/manvscode/wealth/ProfileUpdatedCallback;)V", (void*) profileSetUpdatedCallback },
    { "profileFlags",                 "(J)I", (void*) profileFlags },
    { "profileItemAdd",               "(JILjava/lang/String;D)J", (void*) profileItemAdd },
    { "profileItemRemove",            "(JIJ)Z", (void*) profileItemRemove },
    { "profileItemIndex",             "(JIJ)J", (void*) profileItemIndex },
    { "profileItemGet",               "(JIJ)J", (void*) profileItemGet },
    { "profileItemCount",             "(JI)J", (void*) profileItemCount },
    { "profileItemClear",             "(JI)V", (void*) profileItemClear },
    { "profileClear",                 "(J)V", (void*) profileClear },
    { "profileRefresh",               "(J)V", (void*) profileRefresh },
    { "profileSort",                  "(JI)V", (void*) profileSort },
    { "profileSortItems",             "(JII)V", (void*) profileSortItems },
    { "profileGoal",                  "(J)D", (void*) profileGoal },
    { "profileSetGoal",               "(JD)V", (void*) profileSetGoal },
    { "profileCreditScore",           "(J)S", (void*) profileCreditScore },
    { "profileSetCreditScore",        "(JS)V", (void*) profileSetCreditScore },
    { "profileCreditScoreLastUpdate", "(J)I", (void*) profileCreditScoreLastUpdate },
    { "profileSalary",                "(J)D", (void*) profileSalary },
    { "profileSetSalary",             "(JD)V", (void*) profileSetSalary },
    { "profileMonthlyIncome",         "(J)D", (void*) profileMonthlyIncome },
    { "profileSetMonthlyIncome",      "(JD)V", (void*) profileSetMonthlyIncome },
    { "profileTotalAssets",           "(J)D", (void*) profileTotalAssets },
    { "profileTotalLiabilities",      "(J)D", (void*) profileTotalLiabilities },
    { "profileTotalMonthlyExpenses",  "(J)D", (void*) profileTotalMonthlyExpenses },
    { "profileDisposableIncome",      "(J)D", (void*) profileDisposableIncome },
    { "profileDebtToIncomeRatio",     "(J)D", (void*) profileDebtToIncomeRatio },
    { "profileNetWorth",              "(J)D", (void*) profileNetWorth },
    { "profileProgress",              "(J)F", (void*) profileProgress }
};
static const char* PROFILE_CLASS_NAME = "com/manvscode/wealth/Profile";
static const size_t PROFILE_CLASS_METHOD_COUNT = sizeof(PROFILE_CLASS_METHODS) /  sizeof(PROFILE_CLASS_METHODS[0]);
// ------------------------------------------------
static JNINativeMethod ITEM_CLASS_METHODS[] = {
    { "itemDescription",     "(J)Ljava/lang/String;", (void*) itemDescription },
    { "setItemDescription",  "(JLjava/lang/String;)V", (void*) itemSetDescription },
    { "itemAmount",          "(J)D", (void*) itemAmount },
    { "setItemAmount",       "(JD)V", (void*) itemSetAmount },
};
static const char* ITEM_CLASS_NAME = "com/manvscode/wealth/Item";
static const size_t ITEM_CLASS_METHOD_COUNT = sizeof(ITEM_CLASS_METHODS) /  sizeof(ITEM_CLASS_METHODS[0]);
// ------------------------------------------------
static JNINativeMethod ASSET_CLASS_METHODS[] = {
    { "assetClass",     "(J)I", (void*) assetClass },
    { "assetSetClass",  "(JI)V", (void*) assetSetClass }
};
static const char* ASSET_CLASS_NAME = "com/manvscode/wealth/Asset";
static const size_t ASSET_CLASS_METHOD_COUNT = sizeof(ASSET_CLASS_METHODS) /  sizeof(ASSET_CLASS_METHODS[0]);
// ------------------------------------------------
static JNINativeMethod LIABILITY_CLASS_METHODS[] = {
    { "liabilityClass",     "(J)I", (void*) liabilityClass },
    { "setLiabilityClass",  "(JI)V", (void*) liabilitySetClass },
};
static const char* LIABILITY_CLASS_NAME = "com/manvscode/wealth/Liability";
static const size_t LIABILITY_CLASS_METHOD_COUNT = sizeof(LIABILITY_CLASS_METHODS) /  sizeof(LIABILITY_CLASS_METHODS[0]);



static jint jni_register_native_methods( JNIEnv* env, const char* className, const JNINativeMethod* methods, size_t count )
{
    jclass clazz = (*env)->FindClass( env, className );

    if( clazz == NULL )
    {
        return JNI_ERR;
    }

    if( (*env)->RegisterNatives( env, clazz, methods, count ) < 0 )
    {
        return JNI_ERR;
    }

    return JNI_OK;
}

JNIEnv* jni_getenv( )
{
    JNIEnv* env = NULL;

    if( (*g_vm)->GetEnv(g_vm, (void**) &env, JNI_VERSION_1_4) != JNI_OK )
    {
        LOGE( "GetEnv() failed!" );
        return NULL;
    }

    return env;
}

jint JNI_OnLoad( JavaVM* vm, void* reserved )
{
    JNIEnv* env = NULL;
    g_vm = vm;

    if ((*vm)->GetEnv( vm, (void**) &env, JNI_VERSION_1_6 ) != JNI_OK )
    {
        LOGE( "GetEnv() failed!" );
        return JNI_ERR;
    }


    if( jni_register_native_methods( env, CALC_CLASS_NAME, CALC_CLASS_METHODS, CALC_CLASS_METHOD_COUNT ) != JNI_OK )
    {
        LOGE( "Failed to register methods for %s", CALC_CLASS_NAME );
        return JNI_ERR;
    }

    if( jni_register_native_methods( env, PROFILE_CLASS_NAME, PROFILE_CLASS_METHODS, PROFILE_CLASS_METHOD_COUNT ) != JNI_OK )
    {
        LOGE( "Failed to register methods for %s", PROFILE_CLASS_NAME );
        return JNI_ERR;
    }

    if( jni_register_native_methods( env, ITEM_CLASS_NAME, ITEM_CLASS_METHODS, ITEM_CLASS_METHOD_COUNT ) != JNI_OK )
    {
        LOGE( "Failed to register methods for %s", ITEM_CLASS_NAME );
        return JNI_ERR;
    }

    if( jni_register_native_methods( env, ASSET_CLASS_NAME, ASSET_CLASS_METHODS, ASSET_CLASS_METHOD_COUNT ) != JNI_OK )
    {
        LOGE( "Failed to register methods for %s", ASSET_CLASS_NAME );
        return JNI_ERR;
    }

    if( jni_register_native_methods( env, LIABILITY_CLASS_NAME, LIABILITY_CLASS_METHODS, LIABILITY_CLASS_METHOD_COUNT ) != JNI_OK )
    {
        LOGE( "Failed to register methods for %s", LIABILITY_CLASS_NAME );
        return JNI_ERR;
    }

    library_state.env = env;

    library_state.onProfileUpdatedMethodName      = "onProfileUpdated",
    library_state.onProfileUpdatedMethodSignature = "(com/manvscode/wealth/Profile;I)V",
    library_state.onProfileUpdateCallback         = NULL;

    LOGI( "Library loaded." );
    // Get jclass with env->FindClass.
    // Register methods with env->RegisterNatives.

    return JNI_VERSION_1_6;
}

