#ifndef _LIBSTATE_H_
#define _LIBSTATE_H_

/*
 * Library State for libjava_wealth
 */
typedef struct libstate {
	JNIEnv* env;

	const char* onProfileUpdatedMethodName;
	const char* onProfileUpdatedMethodSignature;
	jobject onProfileUpdateCallback;
} libstate_t;

extern libstate_t library_state;


#endif /* _LIBSTATE_H_ */
