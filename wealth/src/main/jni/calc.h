/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <jni.h>
/* Header for class com_manvscode_wealth_Calc */

#ifndef _Included_com_manvscode_wealth_Calc
#define _Included_com_manvscode_wealth_Calc
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Method:    simpleInterest
 * Signature: (DDD)D
 */
JNIEXPORT jdouble JNICALL simpleInterest(JNIEnv *, jclass, jdouble, jdouble, jdouble);

/*
 * Method:    compoundInterest
 * Signature: (DDD)D
 */
JNIEXPORT jdouble JNICALL compoundInterest(JNIEnv *, jclass, jdouble, jdouble, jdouble);

/*
 * Method:    annuityPresentValue
 * Signature: (DDD)D
 */
JNIEXPORT jdouble JNICALL annuityPresentValue(JNIEnv *, jclass, jdouble, jdouble, jdouble);

/*
 * Method:    annuityDuePresentValue
 * Signature: (DDD)D
 */
JNIEXPORT jdouble JNICALL annuityDuePresentValue(JNIEnv *, jclass, jdouble, jdouble, jdouble);

/*
 * Method:    annuityFutureValue
 * Signature: (DDD)D
 */
JNIEXPORT jdouble JNICALL annuityFutureValue(JNIEnv *, jclass, jdouble, jdouble, jdouble);

/*
 * Method:    annuityDueFutureValue
 * Signature: (DDD)D
 */
JNIEXPORT jdouble JNICALL annuityDueFutureValue(JNIEnv *, jclass, jdouble, jdouble, jdouble);

#ifdef __cplusplus
}
#endif
#endif
