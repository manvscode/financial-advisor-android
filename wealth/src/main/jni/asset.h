/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <jni.h>
/* Header for class com_manvscode_wealth_Asset */

#ifndef _Included_com_manvscode_wealth_Asset
#define _Included_com_manvscode_wealth_Asset
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Method:    assetClass
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL assetClass(JNIEnv *, jobject, jlong);

/*
 * Method:    setAssetClass
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL assetSetClass(JNIEnv *, jobject, jlong, jint);

#ifdef __cplusplus
}
#endif
#endif
