/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <jni.h>
/* Header for class com_manvscode_wealth_Liability */

#ifndef _Included_com_manvscode_wealth_Liability
#define _Included_com_manvscode_wealth_Liability
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Method:    liabilityClass
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL liabilityClass(JNIEnv *, jobject, jlong);

/*
 * Method:    setLiabilityClass
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL liabilitySetClass(JNIEnv *, jobject, jlong, jint);

#ifdef __cplusplus
}
#endif
#endif
