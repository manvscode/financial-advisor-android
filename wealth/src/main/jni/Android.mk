LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)


#build native lib
LOCAL_MODULE           := java_wealth
LOCAL_SRC_FILES        := onload.c \
						  calc.c \
						  profile.c \
						  item.c \
						  asset.c \
						  liability.c

LOCAL_CFLAGS := -DANDROID_NDK -D_DEBUG
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_SHARED_LIBRARIES := collections wealth

include $(BUILD_SHARED_LIBRARY)

include libcollections/projects/android/Android.mk
include libwealth/projects/android/Android.mk
