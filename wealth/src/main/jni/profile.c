/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <stdlib.h>
#include <wealth.h>
#include <android/log.h>
#include "profile.h"
#include "libstate.h"
#include "log.h"


#define STATIC_ASSERT(COND,MSG) typedef char static_assertion_##MSG[(COND)?1:-1]


STATIC_ASSERT( sizeof(jlong) >= sizeof(void*), jlong_ge_pointer );


JNIEXPORT jlong JNICALL profileCreate( JNIEnv* env, jclass clazz )
{
    financial_profile_t* profile = financial_profile_create( );
    LOGI( "Profile created." );
    return (jlong)(intptr_t) profile;
}

JNIEXPORT void JNICALL profileDestroy( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    financial_profile_destroy( &profile );
    LOGI( "Profile destroyed." );
}

JNIEXPORT jlong JNICALL profileLoad( JNIEnv* env, jclass clazz, jstring filename )
{
    const char *cs_filename      = (*env)->GetStringUTFChars( env, filename, NULL );
    financial_profile_t* profile = financial_profile_load( cs_filename );
    LOGI( "Profile loaded from \"%s\" @ %p = ", cs_filename, profile );
    return (jlong)(intptr_t) profile;
}

JNIEXPORT jboolean JNICALL profileSave( JNIEnv* env, jclass clazz, jlong profileHandle, jstring filename )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    const char *cs_filename      = (*env)->GetStringUTFChars( env, filename, NULL );

    jboolean result = financial_profile_save( profile, cs_filename );
    LOGI( "Profile saved at \"%s\".", cs_filename );
    return result;
}


static void internal_profile_updated_callback( const financial_profile_t* profile, flags_t flags, void* user_data )
{
    JNIEnv* env         = library_state.env;
    jclass object_class = (*env)->GetObjectClass( env, library_state.onProfileUpdateCallback );
    jmethodID method_id = (*env)->GetMethodID( env, object_class, library_state.onProfileUpdatedMethodName, library_state.onProfileUpdatedMethodSignature );

    if( 0 == method_id )
    {
        LOGE( "Unable to find method id of \"%s\" with signature = %s.", library_state.onProfileUpdatedMethodName, library_state.onProfileUpdatedMethodSignature );
        return;
    }

    /* Create an instance of a profile. */
    jobject profile_class = (*env)->FindClass( env, "com/manvscode/wealth/Profile" );
    jmethodID profile_constructor = (*env)->GetMethodID( env, profile_class, "<init>", "(J)V" );
    jobject profile_object = (*env)->NewObject( env, profile_class, profile_constructor, profile );

    (*env)->ExceptionClear( env );

    /* Call the callback and pass the profile and the flags. */
    (*env)->CallIntMethod( env, library_state.onProfileUpdateCallback, method_id, profile_object, flags );

    if( (*env)->ExceptionOccurred( env ) )
    {
        LOGE( "Exception occurred!" );
        (*env)->ExceptionClear( env );
    }
}

JNIEXPORT void JNICALL profileSetUpdatedCallback( JNIEnv* env, jclass clazz, jlong profileHandle, jobject callback )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;

    if( callback )
    {
        library_state.onProfileUpdateCallback = callback;
        financial_profile_set_updated_callback( profile, internal_profile_updated_callback );
    }
    else
    {
        library_state.onProfileUpdateCallback = NULL;
        financial_profile_set_updated_callback( profile, NULL );
    }
}

JNIEXPORT jint JNICALL profileFlags( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_flags( profile );
}

JNIEXPORT jlong JNICALL profileItemAdd( JNIEnv* env, jclass clazz, jlong profileHandle, jint type, jstring description, jdouble amount )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    const char *desc             = (*env)->GetStringUTFChars( env, description, NULL );

    financial_item_t* item = financial_profile_item_add( profile, (financial_item_type_t) type, desc, amount );
    jlong itemHandle = (jlong)(intptr_t) item;
    LOGI( "Item added (\"%s\", %.f, handle = %lld, pointer = %p).", desc, amount, itemHandle, item );
    return itemHandle;
}

JNIEXPORT jboolean JNICALL profileItemRemove( JNIEnv* env, jclass clazz, jlong profileHandle, jint type, jlong index )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    jboolean result = (jboolean) financial_profile_item_remove( profile, (financial_item_type_t) type, (size_t) index );
    if( result )
    {
        LOGI( "Item removed (index = %lld).", index );
    }
    else
    {
        LOGE( "Unable to remove item removed (index = %lld).", index );
    }
    return result;
}

JNIEXPORT jlong JNICALL profileItemIndex(JNIEnv* env, jclass clazz, jlong profileHandle, jint type, jlong itemHandle)
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    const financial_item_t* item       = (financial_item_t*)(intptr_t) itemHandle;
    return (jlong)(intptr_t) financial_profile_item_index( profile, (financial_item_type_t) type, item );
}

JNIEXPORT jlong JNICALL profileItemGet( JNIEnv* env, jclass clazz, jlong profileHandle, jint type, jlong id )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return (jlong)(intptr_t) financial_profile_item_get( profile, (financial_item_type_t) type, (size_t) id );
}

JNIEXPORT jlong JNICALL profileItemCount( JNIEnv* env, jclass clazz, jlong profileHandle, jint type )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return (jlong)(intptr_t) financial_profile_item_count( profile, (financial_item_type_t) type );
}

JNIEXPORT void JNICALL profileItemClear( JNIEnv* env, jclass clazz, jlong profileHandle, jint type )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    financial_profile_item_clear( profile, (financial_item_type_t) type );
}

JNIEXPORT void JNICALL profileClear( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    financial_profile_clear( profile );
}

JNIEXPORT void JNICALL profileRefresh( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    financial_profile_refresh( profile );
    LOGI( "Profile refreshed." );
}


JNIEXPORT void JNICALL profileSort( JNIEnv* env, jclass clazz, jlong profileHandle, jint method )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    financial_profile_sort( profile, (financial_item_sort_method_t) method );
    LOGI( "Profile item's sorted." );
}

JNIEXPORT void JNICALL profileSortItems( JNIEnv* env, jclass clazz, jlong profileHandle, jint type, jint method )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    financial_profile_sort_items( profile, (financial_item_type_t) type, (financial_item_sort_method_t) method );
}

JNIEXPORT jdouble JNICALL profileGoal( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_goal( profile );
}

JNIEXPORT void JNICALL profileSetGoal( JNIEnv* env, jclass clazz, jlong profileHandle, jdouble goal )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    LOGI( "Setting goal to $%.2f\n", goal );
    financial_profile_set_goal( profile, goal );
}

JNIEXPORT jshort JNICALL profileCreditScore( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_credit_score( profile );
}

JNIEXPORT void JNICALL profileSetCreditScore( JNIEnv* env, jclass clazz, jlong profileHandle, jshort creditScore )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    LOGI( "Setting credit score to %d\n", creditScore );
    financial_profile_set_credit_score( profile, creditScore );
}

JNIEXPORT jint JNICALL profileCreditScoreLastUpdate(JNIEnv* env, jclass clazz, jlong profileHandle )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_credit_score_last_update( profile );
}

JNIEXPORT jdouble JNICALL profileSalary( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_salary( profile );
}

JNIEXPORT void JNICALL profileSetSalary( JNIEnv* env, jclass clazz, jlong profileHandle, jdouble salary )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    LOGI( "Setting salary to $%.2f\n", salary );
    financial_profile_set_salary( profile, salary );
}

JNIEXPORT jdouble JNICALL profileMonthlyIncome( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_monthly_income( profile );
}

JNIEXPORT void JNICALL profileSetMonthlyIncome( JNIEnv* env, jclass clazz, jlong profileHandle, jdouble monthlyIncome )
{
    financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    LOGI( "Setting monthly income to $%.2f\n", monthlyIncome );
    financial_profile_set_monthly_income( profile, monthlyIncome );
}

JNIEXPORT jdouble JNICALL profileTotalAssets( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_total_assets( profile );
}

JNIEXPORT jdouble JNICALL profileTotalLiabilities( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_total_liabilities( profile );
}

JNIEXPORT jdouble JNICALL profileTotalMonthlyExpenses( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_total_expenses ( profile );
}

JNIEXPORT jdouble JNICALL profileDisposableIncome( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_disposable_income( profile );
}

JNIEXPORT jdouble JNICALL profileDebtToIncomeRatio( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_debt_to_income_ratio( profile );
}

JNIEXPORT jdouble JNICALL profileNetWorth( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_net_worth( profile );
}

JNIEXPORT jfloat JNICALL profileProgress( JNIEnv* env, jclass clazz, jlong profileHandle )
{
    const financial_profile_t* profile = (financial_profile_t*)(intptr_t) profileHandle;
    return financial_profile_progress( profile );
}
