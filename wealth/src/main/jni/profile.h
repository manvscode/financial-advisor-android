/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <jni.h>

#ifndef _Included_com_manvscode_wealth_Profile
#define _Included_com_manvscode_wealth_Profile
#ifdef __cplusplus
extern "C" {
#endif
#undef com_manvscode_wealth_Profile_FP_FLAG_ASSETS_DIRTY
#define com_manvscode_wealth_Profile_FP_FLAG_ASSETS_DIRTY 1L
#undef com_manvscode_wealth_Profile_FP_FLAG_LIABILITIES_DIRTY
#define com_manvscode_wealth_Profile_FP_FLAG_LIABILITIES_DIRTY 2L
#undef com_manvscode_wealth_Profile_FP_FLAG_MONTHLY_EXPENSES_DIRTY
#define com_manvscode_wealth_Profile_FP_FLAG_MONTHLY_EXPENSES_DIRTY 4L
#undef com_manvscode_wealth_Profile_FP_FLAG_INCOME_DIRTY
#define com_manvscode_wealth_Profile_FP_FLAG_INCOME_DIRTY 8L


/*
 * Method:    profileCreate
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL profileCreate(JNIEnv *, jclass);

/*
 * Method:    profileDestroy
 * Signature: (J)V
 */
JNIEXPORT void JNICALL profileDestroy(JNIEnv *, jclass, jlong);

/*
 * Method:    profileLoad
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong JNICALL profileLoad(JNIEnv *, jclass, jstring);

/*
 * Method:    profileSave
 * Signature: (JLjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL profileSave(JNIEnv *, jclass, jlong, jstring);

/*
 * Method:    profileSetUpdatedCallback
 * Signature: (JLcom/manvscode/wealth/ProfileUpdatedCallback;)V
 */
JNIEXPORT void JNICALL profileSetUpdatedCallback(JNIEnv *, jclass, jlong, jobject);

/*
 * Method:    profileFlags
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL profileFlags(JNIEnv *, jclass, jlong);

/*
 * Method:    profileItemAdd
 * Signature: (JILjava/lang/String;D)J
 */
JNIEXPORT jlong JNICALL profileItemAdd(JNIEnv *, jclass, jlong, jint, jstring, jdouble);

/*
 * Method:    profileItemRemove
 * Signature: (JIJ)Z
 */
JNIEXPORT jboolean JNICALL profileItemRemove(JNIEnv *, jclass, jlong, jint, jlong);

/*
 * Method:    itemIndex
 * Signature: (JIJ)J
 */
JNIEXPORT jlong JNICALL profileItemIndex(JNIEnv *, jclass, jlong, jint, jlong);

/*
 * Method:    itemGet
 * Signature: (JIJ)J
 */
JNIEXPORT jlong JNICALL profileItemGet(JNIEnv *, jclass, jlong, jint, jlong);

/*
 * Method:    itemCount
 * Signature: (JI)J
 */
JNIEXPORT jlong JNICALL profileItemCount(JNIEnv *, jclass, jlong, jint);

/*
 * Method:    itemClear
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL profileItemClear(JNIEnv *, jclass, jlong, jint);

/*
 * Method:    clear
 * Signature: (J)V
 */
JNIEXPORT void JNICALL profileClear(JNIEnv *, jclass, jlong);

/*
 * Method:    refresh
 * Signature: (J)V
 */
JNIEXPORT void JNICALL profileRefresh(JNIEnv *, jclass, jlong);

/*
 * Method:    sort
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL profileSort(JNIEnv *, jclass, jlong, jint);

/*
 * Method:    sortItems
 * Signature: (JII)V
 */
JNIEXPORT void JNICALL profileSortItems(JNIEnv *, jclass, jlong, jint, jint);

/*
 * Method:    goal
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileGoal(JNIEnv *, jclass, jlong);

/*
 * Method:    setGoal
 * Signature: (JD)V
 */
JNIEXPORT void JNICALL profileSetGoal(JNIEnv *, jclass, jlong, jdouble);

/*
 * Method:    creditScore
 * Signature: (J)S
 */
JNIEXPORT jshort JNICALL profileCreditScore(JNIEnv *, jclass, jlong);

/*
 * Method:    setCreditScore
 * Signature: (JS)V
 */
JNIEXPORT void JNICALL profileSetCreditScore(JNIEnv *, jclass, jlong, jshort);

/*
 * Method:    creditScoreLastUpdate
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL profileCreditScoreLastUpdate(JNIEnv *, jclass, jlong);

/*
 * Method:    salary
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileSalary(JNIEnv *, jclass, jlong);

/*
 * Method:    setSalary
 * Signature: (JD)V
 */
JNIEXPORT void JNICALL profileSetSalary(JNIEnv *, jclass, jlong, jdouble);

/*
 * Method:    monthlyIncome
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileMonthlyIncome(JNIEnv *, jclass, jlong);

/*
 * Method:    setMonthlyIncome
 * Signature: (JD)V
 */
JNIEXPORT void JNICALL profileSetMonthlyIncome(JNIEnv *, jclass, jlong, jdouble);

/*
 * Method:    totalAssets
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileTotalAssets(JNIEnv *, jclass, jlong);

/*
 * Method:    totalLiabilities
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileTotalLiabilities(JNIEnv *, jclass, jlong);

/*
 * Method:    totalMonthlyExpenses
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileTotalMonthlyExpenses(JNIEnv *, jclass, jlong);

/*
 * Method:    disposableIncome
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileDisposableIncome(JNIEnv *, jclass, jlong);

/*
 * Method:    debtToIncomeRatio
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileDebtToIncomeRatio(JNIEnv *, jclass, jlong);

/*
 * Method:    netWorth
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL profileNetWorth(JNIEnv *, jclass, jlong);

/*
 * Method:    progress
 * Signature: (J)F
 */
JNIEXPORT jfloat JNICALL profileProgress(JNIEnv *, jclass, jlong);


#ifdef __cplusplus
}
#endif
#endif
