/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <jni.h>
/* Header for class com_manvscode_wealth_Item */

#ifndef _Included_com_manvscode_wealth_Item
#define _Included_com_manvscode_wealth_Item
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Method:    itemDescription
 * Signature: (J)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL itemDescription(JNIEnv *, jclass, jlong);

/*
 * Method:    setItemDescription
 * Signature: (JLjava/lang/String;)V
 */
JNIEXPORT void JNICALL itemSetDescription(JNIEnv *, jclass, jlong, jstring);

/*
 * Method:    itemAmount
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL itemAmount(JNIEnv *, jclass, jlong);

/*
 * Method:    setItemAmount
 * Signature: (JD)V
 */
JNIEXPORT void JNICALL itemSetAmount(JNIEnv *, jclass, jlong, jdouble);

#ifdef __cplusplus
}
#endif
#endif
