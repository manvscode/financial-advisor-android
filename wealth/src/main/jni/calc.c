/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <stdlib.h>
#include <wealth.h>
#include <android/log.h>
#include "calc.h"
#include "log.h"


JNIEXPORT jdouble JNICALL simpleInterest(JNIEnv* env, jclass cls, jdouble principle, jdouble rate, jdouble time)
{
	return simple_interest( principle, rate, time );
}

JNIEXPORT jdouble JNICALL compoundInterest(JNIEnv* env, jclass cls, jdouble principle, jdouble rate, jdouble time)
{
	return compound_interest( principle, rate, time );
}

JNIEXPORT jdouble JNICALL annuityPresentValue(JNIEnv* env, jclass cls, jdouble amount, jdouble rate, jdouble time)
{
	return annuity_present_value( amount, rate, time );
}

JNIEXPORT jdouble JNICALL annuityDuePresentValue(JNIEnv* env, jclass cls, jdouble amount, jdouble rate, jdouble time)
{
	return annuity_due_present_value( amount, rate, time );
}

JNIEXPORT jdouble JNICALL annuityFutureValue(JNIEnv* env, jclass cls, jdouble amount, jdouble rate, jdouble time)
{
	return annuity_future_value( amount, rate, time );
}

JNIEXPORT jdouble JNICALL annuityDueFutureValue(JNIEnv* env, jclass cls, jdouble amount, jdouble rate, jdouble time)
{
	return annuity_due_future_value( amount, rate, time );
}
