/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <stdlib.h>
#include <wealth.h>
#include <android/log.h>
#include "liability.h"
#include "log.h"

JNIEXPORT jint JNICALL liabilityClass( JNIEnv* env, jobject cls, jlong itemHandle )
{
	const financial_liability_t* liability = (const financial_liability_t*)(intptr_t) itemHandle;
	return (jint) financial_liability_class( liability );
}

JNIEXPORT void JNICALL liabilitySetClass( JNIEnv* env, jobject cls, jlong itemHandle, jint liability_class )
{
	financial_liability_t* liability = (financial_liability_t*)(intptr_t) itemHandle;
	LOGI( "Setting liability class to %d in liability %p\n", liability_class, liability );
	financial_liability_set_class( liability, (financial_liability_class_t) liability_class );
}
