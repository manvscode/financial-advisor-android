/*
 * Copyright (C) 2015 Joseph A. Marrero. http://www.manvscode.com/
 * All rights reserved.
 */
#include <stdlib.h>
#include <wealth.h>
#include <android/log.h>
#include "asset.h"
#include "log.h"

JNIEXPORT jint JNICALL assetClass( JNIEnv* env, jobject cls, jlong itemHandle )
{
	const financial_asset_t* asset = (const financial_asset_t*)(intptr_t) itemHandle;
	return (jint) financial_asset_class( asset );
}

JNIEXPORT void JNICALL assetSetClass( JNIEnv* env, jobject cls, jlong itemHandle, jint asset_class )
{
	financial_asset_t* asset = (financial_asset_t*)(intptr_t) itemHandle;
	LOGI( "Setting asset class to %d in asset %p\n", asset_class, asset );
	financial_asset_set_class( asset, (financial_asset_class_t) asset_class );
}
