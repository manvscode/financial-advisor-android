package com.manvscode.wealth;


public class Calc
{
    public static native double simpleInterest( double principle, double rate, double time );
    public static native double compoundInterest( double principle, double rate, double time );
    public static native double annuityPresentValue( double amount, double rate, double time );
    public static native double annuityDuePresentValue( double amount, double rate, double time );
    public static native double annuityFutureValue( double amount, double rate, double time );
    public static native double annuityDueFutureValue( double amount, double rate, double time );


    static {
        System.loadLibrary( "wealth" );
        System.loadLibrary( "java_wealth" );
    }
}
