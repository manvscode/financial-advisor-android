package com.manvscode.wealth;


public enum LiabilityClass
{
    Unspecified(0),
    Current(1),
    LongTerm(2);


    public static final  String[] STRINGS = {
            "Unspecified",
            "Current",
            "Long Term"
    };

    private int value;

    private LiabilityClass( int value )
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    public String toString( )
    {
        return STRINGS[ getValue() ];
    }
}
