package com.manvscode.wealth;


public interface ProfileUpdatedCallback
{

    public void onProfileUpdated( Profile p, int flags );
}
