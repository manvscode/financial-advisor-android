package com.manvscode.wealth;


public enum AssetClass
{
    Unspecified(0),
    Cash(1),
    Equity(2), /* Stocks, options, futures, et cetera */
    FixedIncome(3), /* Bonds */
    MoneyMarket(4),
    RealEstate(5),
    Guaranteed(6),
    Commodities(7);


    public static final  String[] STRINGS = {
            "Unspecified",
            "Cash",
            "Equity",
            "Fixed Income",
            "Money Market",
            "Real Estate",
            "Guaranteed",
            "Commodities"
    };

    private int value;

    private AssetClass( int value )
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    public String toString( )
    {
        return STRINGS[ getValue() ];
    }
}
