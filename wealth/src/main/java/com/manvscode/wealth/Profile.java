/*
 * Copyright (C) 2015 Joseph A. Marrero.  http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.manvscode.wealth;


import android.util.Log;

public class Profile
{
    public static final int FP_FLAG_ASSETS_DIRTY           = (1 << 0);
    public static final int FP_FLAG_LIABILITIES_DIRTY      = (1 << 1);
    public static final int FP_FLAG_MONTHLY_EXPENSES_DIRTY = (1 << 2);
    public static final int FP_FLAG_INCOME_DIRTY           = (1 << 3);

    protected long mProfileHandle;
    private boolean mIsDisposed = false;
    protected boolean mIsDemoMode = false;
    protected ProfileUpdatedCallback mUpdatedCallback = null;


    public static Profile newInstance()
    {
        long handle = profileCreate( );
        Profile result = null;

        try
        {
            result = new Profile( handle );
        }
        catch( IllegalArgumentException e )
        {
            Log.e( "Profile", e.getMessage() );
        }

        return result;
    }

    public static Profile demo()
    {
        Profile profile = Profile.newInstance();
        profile.mIsDemoMode = true;

        profile.setMonthlyIncome( 6400.0 );
        profile.setGoal( 1000000 );
        profile.setCreditScore( (short) 720 );

        // Assets
        {
            Asset asset = null;
            asset = (Asset) profile.addItem( ItemType.Asset, "House", 162900.0 );
            asset.setAssetClass( AssetClass.RealEstate );
            asset = (Asset) profile.addItem( ItemType.Asset, "Chase Checking Account", 1200.0 );
            asset.setAssetClass( AssetClass.Cash );
            asset = (Asset) profile.addItem( ItemType.Asset, "2005 Jeep Wrangler", 12271.0 );
            asset.setAssetClass( AssetClass.Guaranteed );
            asset = (Asset) profile.addItem( ItemType.Asset, "Fidelity: Investment", 10200.1 );
            asset.setAssetClass( AssetClass.Equity );
            asset = (Asset) profile.addItem( ItemType.Asset, "Fidelity: Simple IRA", 31064.57 );
            asset.setAssetClass( AssetClass.Equity );
            asset = (Asset) profile.addItem( ItemType.Asset, "Silver Bullion", 140.0 );
            asset.setAssetClass( AssetClass.Commodities );
            asset = (Asset) profile.addItem( ItemType.Asset, "Cash", 500.0 );
            asset.setAssetClass( AssetClass.Cash );
            asset = (Asset) profile.addItem( ItemType.Asset, "PayPal", 1421.0 );
            asset.setAssetClass( AssetClass.Cash );
            asset = (Asset) profile.addItem( ItemType.Asset, "Fidelity: Roth IRA", 1968.70 );
            asset.setAssetClass( AssetClass.Equity );
            asset = (Asset) profile.addItem( ItemType.Asset, "Fidelity: Rollover IRA", 12584.17 );
            asset.setAssetClass( AssetClass.Equity );
        }

        // Liabilities
        {
            Liability liability = null;
            liability = (Liability) profile.addItem( ItemType.Liability, "Capital One Credit Card", 0.0 );
            liability.setLiabilityClass( LiabilityClass.Current );
            liability = (Liability) profile.addItem( ItemType.Liability, "Chase Freedom Credit Card", 500.0 );
            liability.setLiabilityClass( LiabilityClass.Current );
            liability = (Liability) profile.addItem( ItemType.Liability, "American Express Credit Card", 0.0 );
            liability.setLiabilityClass( LiabilityClass.Current );
            liability = (Liability) profile.addItem( ItemType.Liability, "Mortgage", 104342.0 );
            liability.setLiabilityClass( LiabilityClass.LongTerm );
        }

        // Expenses
        {
            MonthlyExpense monthlyExpense = null;
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "FPL", 115.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Gym", 66.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Comcast", 65.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Progressive Auto Insurance", 160.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Rent", 995.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Amazon Prime", 8.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Amazon Web Services", 52.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Food (Groceries and fast food)", 400.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Entertainment", 400.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Tolls", 70.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Gas/Fuel", 400.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Haircut", 30.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "Miscellaneous", 100.0 );
            monthlyExpense = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, "T-Mobile Service", 100.0 );
        }

        profile.refresh();
        profile.sort( ItemSort.AmountDescending );

        return profile;
    }

    public static Profile load( String filename )
    {
        long handle = profileLoad( filename );
        Profile result = null;

        try
        {
            result = new Profile( handle );
        }
        catch( IllegalArgumentException e )
        {
            Log.e( "Profile", e.getMessage() );
        }

        return result;
    }

    public Profile( long handle ) throws IllegalArgumentException
    {
        if( 0 == handle ) throw new IllegalArgumentException( "Bad handle!" );
        mProfileHandle = handle;
    }


    public long handle()
    {
        return mProfileHandle;
    }

    public void setOnUpdateCallback( ProfileUpdatedCallback c )
    {
        mUpdatedCallback = c;
        // Set object to be called from JNI.
        profileSetUpdatedCallback( mProfileHandle, c );
    }

    public int flags( )
    {
        return profileFlags( mProfileHandle );
    }

    @Override
    protected void finalize() throws Throwable
    {
        dispose( );
    }

    protected synchronized boolean isDispose( )
    {
        return mIsDisposed;
    }

    public synchronized void dispose( )
    {
        if( mIsDisposed )
        {
            profileDestroy( mProfileHandle );
            mIsDisposed = true;
        }
    }

    public boolean save( String filename )
    {
        boolean result = true;

        if( !mIsDemoMode )
        {
            result = profileSave( mProfileHandle, filename );
        }

        return result;
    }

    public Item addItem( ItemType type, String description, double value )
    {
        long itemHandle = profileItemAdd( mProfileHandle, type.getValue(), description, value );
        Item result = null;

        switch( type )
        {
            case Asset:
                result = new Asset( itemHandle );
                break;
            case Liability:
                result = new Liability( itemHandle );
                break;
            case MonthlyExpense:
                result = new MonthlyExpense( itemHandle );
                break;
            default:
                break;
        }

        return result;
    }

    public boolean removeItem( ItemType type, long index )
    {
        return profileItemRemove( mProfileHandle, type.getValue(), index );
    }

    public long indexOfItem( ItemType type, Item item )
    {
        return profileItemIndex( mProfileHandle, type.getValue(), item.handle() );
    }

    public Item getItem( ItemType type, long index )
    {
        long count = countItems( type );
        assert( index < count );
        long itemHandle = profileItemGet( mProfileHandle, type.getValue(), index );
        Item result = null;

        switch( type )
        {
            case Asset:
                result = new Asset( itemHandle );
                break;
            case Liability:
                result = new Liability( itemHandle );
                break;
            case MonthlyExpense:
                result = new MonthlyExpense( itemHandle );
                break;
            default:
                break;
        }

        return result;
    }

    public long countItems( ItemType type )
    {
        return profileItemCount( mProfileHandle, type.getValue() );
    }

    public void clearItems( ItemType type )
    {
        profileItemClear( mProfileHandle, type.getValue() );
    }

    public void clear( )
    {
        profileClear( mProfileHandle );
    }

    public void refresh( )
    {
        profileRefresh( mProfileHandle );
    }

    public void sort( ItemSort method )
    {
        profileSort( mProfileHandle, method.getValue() );
    }

    public void sortItems( ItemType type, ItemSort method )
    {
        profileSortItems( mProfileHandle, type.getValue(), method.getValue() );
    }

    public double goal( )
    {
        return profileGoal( mProfileHandle );
    }

    public void setGoal( double goal )
    {
        profileSetGoal( mProfileHandle, goal );
    }

    public double toGoal( )
    {
        double g  = this.goal();
        double nw = this.netWorth();

        return ( g > nw ) ? (g - nw) : 0.0;
    }

    public short creditScore()
    {
        return profileCreditScore( mProfileHandle );
    }

    public void setCreditScore(short score)
    {
        profileSetCreditScore( mProfileHandle, score );
    }

    public int creditScoreLastUpdate()
    {
        return profileCreditScoreLastUpdate( mProfileHandle );
    }

    public double salary( )
    {
        return profileSalary( mProfileHandle );
    }

    public void setSalary( double salary )
    {
        profileSetSalary( mProfileHandle, salary );
    }

    public double monthlyIncome( )
    {
        return profileMonthlyIncome( mProfileHandle );
    }

    public void setMonthlyIncome( double monthlyIncome )
    {
        profileSetMonthlyIncome( mProfileHandle, monthlyIncome );
    }

    public double totalAssets( )
    {
        return profileTotalAssets( mProfileHandle );
    }

    public double totalLiabilities( )
    {
        return profileTotalLiabilities( mProfileHandle );
    }

    public double totalMonthlyExpenses( )
    {
        return profileTotalMonthlyExpenses( mProfileHandle );
    }

    public double disposableIncome( )
    {
        return profileDisposableIncome( mProfileHandle );
    }

    public double debtToIncomeRatio( )
    {
        return profileDebtToIncomeRatio( mProfileHandle );
    }

    public double netWorth( )
    {
        return profileNetWorth( mProfileHandle );
    }

    public float progress( )
    {
        return profileProgress( mProfileHandle );
    }


    protected static native long    profileCreate( );
    protected static native void    profileDestroy( long profileHandle );
    protected static native long    profileLoad( String filename );
    protected static native boolean profileSave( long profileHandle, String filename );
    protected static native void    profileSetUpdatedCallback( long profileHandle, ProfileUpdatedCallback callback );
    protected static native int     profileFlags( long profileHandle );
    protected static native long    profileItemAdd( long profileHandle, int type, String description, double value );
    protected static native boolean profileItemRemove( long profileHandle, int type, long index );
    protected static native long    profileItemIndex( long profileHandle, int type, long itemHandle );
    protected static native long    profileItemGet( long profileHandle, int type, long index );
    protected static native long    profileItemCount( long profileHandle, int type );
    protected static native void    profileItemClear( long profileHandle, int type );
    protected static native void    profileClear( long profileHandle );
    protected static native void    profileRefresh( long profileHandle );
    protected static native void    profileSort( long profileHandle, int method );
    protected static native void    profileSortItems( long profileHandle, int type, int method );
    protected static native double  profileGoal( long profileHandle );
    protected static native void    profileSetGoal( long profileHandle, double goal );
    protected static native short   profileCreditScore( long profileHandle );
    protected static native void    profileSetCreditScore( long profileHandle, short score );
    protected static native int     profileCreditScoreLastUpdate( long profileHandle );
    protected static native double  profileSalary( long profileHandle );
    protected static native void    profileSetSalary( long profileHandle, double salary );
    protected static native double  profileMonthlyIncome( long profileHandle );
    protected static native void    profileSetMonthlyIncome( long profileHandle, double monthlyIncome );
    protected static native double  profileTotalAssets( long profileHandle );
    protected static native double  profileTotalLiabilities( long profileHandle );
    protected static native double  profileTotalMonthlyExpenses( long profileHandle );
    protected static native double  profileDisposableIncome( long profileHandle );
    protected static native double  profileDebtToIncomeRatio( long profileHandle );
    protected static native double  profileNetWorth( long profileHandle );
    protected static native float   profileProgress( long profileHandle );

    static {
        System.loadLibrary( "wealth" );
        System.loadLibrary( "java_wealth" );
    }
}
