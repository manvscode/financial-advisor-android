package com.manvscode.wealth;


public class Item
{
    protected long mItemHandle;

    public Item( long handle )
    {
        mItemHandle = handle;
    }

    public long handle()
    {
        return mItemHandle;
    }

    public String description( )
    {
        return itemDescription( mItemHandle );
    }

    public void setDescription( String description )
    {
        setItemDescription( mItemHandle, description );
    }

    public double amount( )
    {
        return itemAmount( mItemHandle );
    }

    public void setAmount( double amount )
    {
        setItemAmount( mItemHandle, amount );
    }


    protected static native String itemDescription( long itemHandle );
    protected static native void   setItemDescription( long itemHandle, String description );
    protected static native double itemAmount( long itemHandle );
    protected static native void   setItemAmount( long itemHandle, double amount );

    static {
        System.loadLibrary( "wealth" );
        System.loadLibrary( "java_wealth" );
    }
}
