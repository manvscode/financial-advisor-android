package com.manvscode.wealth;

public enum ItemSort
{
    DescriptionAscending(0),
    DescriptionDescending(1),
    AmountAscending(2),
    AmountDescending(3);

    private int value;

    private ItemSort( int value )
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }
}
