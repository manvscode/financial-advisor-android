package com.manvscode.wealth;


public class Asset extends Item
{

    public Asset( long handle )
    {
        super(handle);
    }

    public AssetClass assetClass( )
    {
        int ac = assetClass(handle());
        if( ac < 0 )
        {
            return AssetClass.Unspecified;
        }
        else
        {
            return AssetClass.values( )[ac];
        }
    }

    public void setAssetClass( AssetClass cls )
    {
        assetSetClass( handle(), cls.getValue()  );
    }

    protected native int assetClass( long itemHandle );
    protected native void assetSetClass( long itemHandle, int cls );

}
