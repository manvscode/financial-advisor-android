package com.manvscode.wealth;



public class Liability extends Item
{
    public Liability( long handle )
    {
        super( handle );
    }

    public LiabilityClass liabilityClass( )
    {
        int c = liabilityClass( handle() );
        if( c < 0 )
        {
            return LiabilityClass.Unspecified;
        }
        else
        {
            return LiabilityClass.values( )[c];
        }
    }

    public void setLiabilityClass( LiabilityClass cls )
    {
        setLiabilityClass( handle(), cls.getValue() );
    }

    protected native int liabilityClass( long itemHandle );
    protected native void setLiabilityClass( long itemHandle, int cls );
}
