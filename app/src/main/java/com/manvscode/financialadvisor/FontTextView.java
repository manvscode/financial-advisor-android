package com.manvscode.financialadvisor;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView
{
    public FontTextView( Context ctx )
    {
        super(ctx);
        setTypeface( FontUtil.getTypeface( ctx, FontUtil.FontType.CONTENT_FONT ) );
    }

    public FontTextView(Context ctx, AttributeSet attrs)
    {
        super( ctx, attrs );
        setTypeface( FontUtil.getTypeface( ctx, FontUtil.FontType.CONTENT_FONT ) );
    }

    public FontTextView( Context ctx, AttributeSet attrs, int defStyleAttr )
    {
        super( ctx, attrs, defStyleAttr );
        setTypeface( FontUtil.getTypeface( ctx, FontUtil.FontType.CONTENT_FONT ) );
    }

}
