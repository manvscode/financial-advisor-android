package com.manvscode.financialadvisor;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtil
{
    private static Typeface mTitleFont = null;
    private static Typeface mContentFont = null;
    private static Typeface mDrawerFont = null;

    public static enum FontType
    {

        TITLE_FONT {
            public String toString() {
                return "mesmerize-el.ttf";
            }
        },

        CONTENT_FONT {
            public String toString() {
                return "opensans.ttf";
            }
        },
    }

    /**
     * @return Typeface Instance with the font passed as parameter
     */
    public static Typeface getTypeface(Context context, String typefaceName)
    {
        Typeface typeFace = null;

        try
        {
            if (typefaceName.equals(FontType.TITLE_FONT.toString()))
            {
                if (mTitleFont == null)
                {
                    mTitleFont = Typeface.createFromAsset(
                            context.getAssets(), "fonts/" + typefaceName);
                }
                typeFace = mTitleFont;
            }
            else if (typefaceName.equals(FontType.CONTENT_FONT.toString()))
            {
                if (mContentFont == null)
                {
                    mContentFont = Typeface.createFromAsset(
                            context.getAssets(), "fonts/" + typefaceName);
                }
                typeFace = mContentFont;
            }
            /*
            else if (typefaceName.equals(FontType.DRAWER_FONT.toString()))
            {
                if (mDrawerFont == null)
                {
                    mDrawerFont = Typeface.createFromAsset(
                            context.getAssets(), "fonts/" + typefaceName);
                }
                typeFace = mDrawerFont;
            }
            */

        }
        catch (Exception ex)
        {
            typeFace = Typeface.DEFAULT;
        }

        return typeFace;
    }

    /**
     * @return Typeface Instance with the font passed as parameter
     */
    public static Typeface getTypeface(Context context, FontType typefaceName)
    {
        return getTypeface(context, typefaceName.toString());
    }
}