package com.manvscode.financialadvisor.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.manvscode.financialadvisor.FontUtil;
import com.manvscode.financialadvisor.R;

public class DrawerAdapter extends ArrayAdapter<String>
{
    public DrawerAdapter(Context ctx, String[] items)
    {
        super(ctx, R.layout.drawer_item, android.R.id.text1, items);
    }



    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = super.getView( position, convertView, parent );

        //TextView tv = (TextView) view.findViewById( android.R.id.text1 );
        //tv.setTypeface( FontUtil.getTypeface( view.getContext(), FontUtil.FontType.TITLE_FONT) );

        return view;
    }
}
