//-------------------------------------------------------------
// Copyright (C) 2015 Joseph A. Marrero.  All rights reserved.
// http://www.manvscode.com/
//-------------------------------------------------------------
package com.manvscode.financialadvisor.adapters;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.manvscode.financialadvisor.R;

import java.util.Arrays;
import java.util.List;

public abstract class TwoLineListAdapter<T> extends ArrayAdapter<T>
{
    protected OnClickListener mOnClickListener = null;

    public TwoLineListAdapter( Context c, List<T> list )
    {
        this( c, list, null );
    }

    public TwoLineListAdapter( Context c, T[] array )
    {
        this( c, array, null );
    }

    public TwoLineListAdapter( Context c, T[] array, OnClickListener onClickListener )
    {
        this( c, Arrays.asList(array), onClickListener );
    }

    public TwoLineListAdapter( Context c, List<T> list, OnClickListener onClickListener )
    {
        super( c, R.layout.twoline_item, android.R.id.text1, list );
        mOnClickListener = onClickListener;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        return super.getView( position, convertView, parent );
    }

    public abstract String lineOne( T t );
    public abstract String lineTwo( T t );


    /**
     * Interface for click events.
     * @param <T>
     */
    public interface OnClickListener<T>
    {
        public void onClick( T t );
    }
}
