package com.manvscode.financialadvisor.adapters;


import android.content.Context;
import android.widget.BaseAdapter;
;
import com.manvscode.wealth.Item;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.Profile;

public abstract class ItemsAdapter extends BaseAdapter
{
    protected Context mContext;
    protected Profile mProfile;
    protected ItemType mItemType;


    public ItemsAdapter( Context context, Profile profile, ItemType itemType )
    {
        mContext  = context;
        mProfile  = profile;
        mItemType = itemType;
    }

    public Context getContext()
    {
        return mContext;
    }

    public Profile getProfile()
    {
        return mProfile;
    }

    public ItemType getItemType()
    {
        return mItemType;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     * data set.
     * @return The data at the specified position.
     */
    public Object getItem(int position)
    {
        return mProfile.getItem( mItemType, (long) position );
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    public long getItemId(int position)
    {
        Item item = mProfile.getItem( mItemType, (long) position );
        return item.handle();
    }
    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    public int getCount()
    {
        return (int) mProfile.countItems( mItemType );
    }

    /**
     * @return true if this adapter doesn't contain any data.  This is used to determine
     * whether the empty view should be displayed.  A typical implementation will return
     * getCount() == 0 but since getCount() includes the headers and footers, specialized
     * adapters might want a different behavior.
     */
    public boolean isEmpty()
    {
        return mProfile.countItems( mItemType ) <= 0;
    }
}
