package com.manvscode.financialadvisor.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.MonthlyExpense;
import com.manvscode.wealth.Profile;

public class MonthlyExpensesAdapter extends ItemsAdapter
{
    protected OnMonthlyExpenseEditListener mEditListener = null;
    protected boolean mEditMode = false;
    protected ImageButton mDeleteButton = null;

    public interface OnMonthlyExpenseEditListener {
        void onMonthlyExpenseEdit( MonthlyExpense asset );
    }

    public MonthlyExpensesAdapter( Context context, Profile profile, OnMonthlyExpenseEditListener editListener )
    {
        super( context, profile, ItemType.MonthlyExpense );
        mEditListener = editListener;
    }


    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position The position of the item within the adapter's data set of the item whose view
     *        we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *        is non-null and of an appropriate type before using. If it is not possible to convert
     *        this view to display the correct data, this method can create a new view.
     *        Heterogeneous lists can specify their number of view types, so that this View is
     *        always of the right type (see {@link #getViewTypeCount()} and
     *        {@link #getItemViewType(int)}).
     * @param parent The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View layout = inflater.inflate( R.layout.item_liability, null );

        layout.setBackgroundColor( mContext.getResources().getColor(position % 2 == 0 ? R.color.odd_color : R.color.even_color) );

        final long count = mProfile.countItems( ItemType.MonthlyExpense );
        final long index = (long) position;
        final MonthlyExpense monthlyExpense = (MonthlyExpense) mProfile.getItem( ItemType.MonthlyExpense, (long) position );

        TextView description = (TextView) layout.findViewById( R.id.item_liability_description );
        description.setText( monthlyExpense.description() );

        TextView amount = (TextView) layout.findViewById( R.id.item_liability_amount );
        amount.setText( Utils.formatCurrency( monthlyExpense.amount(), 2 ) );

        //TextView expenseClass = (TextView) layout.findViewById( R.id.item_liability_class );
        //expenseClass.setText( Utils.liabilityClassString( getContext(), monthlyExpense.expenseClass() ) );

        mDeleteButton = (ImageButton) layout.findViewById( R.id.item_liability_delete_button );
        mDeleteButton.setVisibility( mEditMode ? View.VISIBLE : View.GONE );
        mDeleteButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                final String description = monthlyExpense.description();
                boolean result = DataLayer.get().getProfile().removeItem( ItemType.MonthlyExpense, index );
                if( result )
                {
                    Toast.makeText(mContext, String.format( getContext().getString(R.string.toast_item_removed), description), Toast.LENGTH_SHORT).show();
                }
                notifyDataSetChanged();
            }
        } );

        layout.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                if( mEditMode && mEditListener != null )
                {
                    mEditListener.onMonthlyExpenseEdit( monthlyExpense );
                }
            }
        } );

        return layout;
    }

    public void setEditMode( boolean mode )
    {
        mEditMode = mode;
        notifyDataSetChanged();
    }

    public void toggleEditMode( )
    {
        setEditMode( !mEditMode );
    }

    public boolean isEditMode()
    {
        return mEditMode;
    }

}
