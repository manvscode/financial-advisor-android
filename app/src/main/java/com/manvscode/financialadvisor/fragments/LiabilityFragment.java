package com.manvscode.financialadvisor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.Liability;
import com.manvscode.wealth.LiabilityClass;
import com.manvscode.wealth.Profile;


public class LiabilityFragment extends DialogFragment
{
    private static final String TAG        = "LiabilityFragment";
    private static final String ARG_HANDLE = "handle";
    protected Liability mLiability   = null;
    protected String mDescription    = "";
    protected double mAmount         = 0.0;
    protected LiabilityClass mLiabilityClass = LiabilityClass.Unspecified;



    public static LiabilityFragment newInstance( Liability item )
    {
        LiabilityFragment fragment = new LiabilityFragment( );
        Bundle args = new Bundle( );
        args.putLong( ARG_HANDLE, item.handle() );
        fragment.setArguments( args );
        return fragment;
    }

    public static LiabilityFragment newInstance( )
    {
        LiabilityFragment fragment = new LiabilityFragment( );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if( getArguments( ) != null )
        {
            long handle = getArguments().getLong( ARG_HANDLE );

            if( handle == 0L )
            {
                mLiability      = null;
                mDescription    = "";
                mAmount         = 0.0;
                mLiabilityClass = LiabilityClass.Unspecified;
            }
            else
            {
                mLiability      = new Liability( handle );
                mDescription    = mLiability.description();
                mAmount         = mLiability.amount();
                mLiabilityClass = mLiability.liabilityClass();
            }
        }
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        String title = isEditing() ? getString( R.string.update_liability ) : getString( R.string.create_liability );
        Dialog dlg = getDialog();

        if( dlg != null )
        {
            dlg.setTitle( title );
            dlg.setCancelable( true );
            //int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
            //int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
            dlg.getWindow().setLayout( Utils.convertDipsToPixels( 200 ), Utils.convertDipsToPixels( 200 ) );
        }

        View layout = inflater.inflate( R.layout.fragment_liability, container, false );

        final EditText descriptionEditText = (EditText) layout.findViewById( R.id.liability_description_edit_text );
        final EditText amountEditText = (EditText) layout.findViewById( R.id.liability_amount_edit_text );

        String[] classArray = getResources().getStringArray( R.array.liability_classes );
        final Spinner classSpinner = (Spinner) layout.findViewById( R.id.liability_class_spinner );
        classSpinner.setAdapter( new ArrayAdapter<String>( getActivity(), android.R.layout.simple_dropdown_item_1line, classArray ) );
        classSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
            {
                mLiabilityClass = LiabilityClass.values()[position];
            }

            @Override
            public void onNothingSelected( AdapterView<?> parent )
            {
            }
        } );

        if( isEditing() )
        {
            descriptionEditText.setText( mDescription );
            amountEditText.setText( Double.toString( mAmount ) );
            classSpinner.setSelection( mLiabilityClass.getValue() );
        }

        Button saveButton = (Button) layout.findViewById( R.id.liability_done_button );
        saveButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Profile profile = DataLayer.get().getProfile();
                mDescription = descriptionEditText.getText().toString();
                try
                {
                    mAmount = Double.parseDouble( amountEditText.getText().toString() );
                }
                catch( Exception e )
                {
                    mAmount = 0.0;
                }

                if( isEditing() )
                {
                    mLiability.setDescription( mDescription );
                    mLiability.setAmount( mAmount );
                    mLiability.setLiabilityClass( mLiabilityClass );

                    Toast.makeText( getActivity(), getString( R.string.toast_liability_saved), Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    Liability item = (Liability) profile.addItem( ItemType.Liability, mDescription, mAmount );
                    item.setLiabilityClass( mLiabilityClass );
                    Toast.makeText( getActivity(), getString( R.string.toast_liability_created), Toast.LENGTH_SHORT ).show();
                }

                profile.refresh();

                close();
            }
        } );

        Button cancelButton = (Button) layout.findViewById( R.id.liability_cancel_button );
        cancelButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                close();
            }
        } );
        return layout;
    }

    public void close( )
    {
        Dialog dlg = getDialog();

        if( dlg == null )
        {
            getActivity().finish();
        }
        else
        {
            LiabilityFragment.this.dismiss();
        }
    }

    public boolean isEditing()
    {
        return mLiability != null;
    }

}
