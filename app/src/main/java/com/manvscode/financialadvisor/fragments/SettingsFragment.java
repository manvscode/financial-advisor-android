package com.manvscode.financialadvisor.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.Profile;

public class SettingsFragment extends Fragment
{

    public static SettingsFragment newInstance(  )
    {
        SettingsFragment fragment = new SettingsFragment( );
        return fragment;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        final Profile profile = DataLayer.get().getProfile();
        // Inflate the layout for this fragment
        View layout = inflater.inflate( R.layout.fragment_settings, container, false );

        final EditText monthlyIncomeValue = (EditText) layout.findViewById( R.id.settings_monthly_income );
        monthlyIncomeValue.setText( Utils.numberToString( profile.monthlyIncome() ) );

        final EditText creditScoreValue = (EditText) layout.findViewById( R.id.settings_credit_score );
        creditScoreValue.setText( Utils.numberToString( profile.creditScore() ) );


        Button saveButton = (Button) layout.findViewById( R.id.settings_done_button );
        saveButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                try
                {
                    profile.setMonthlyIncome( Double.parseDouble( monthlyIncomeValue.getText().toString() ) );
                }
                catch( Exception e )
                {
                    profile.setMonthlyIncome( 0.0 );
                }
                try
                {
                    profile.setCreditScore( Short.parseShort( creditScoreValue.getText().toString() ) );
                }
                catch( Exception e )
                {
                    profile.setCreditScore( (short)0 );
                }

                profile.refresh();
                Toast.makeText(getActivity(), getString(R.string.toast_settings_saved), Toast.LENGTH_SHORT).show();
            }
        } );

        ToggleButton demoButton = (ToggleButton) layout.findViewById( R.id.settings_demo_mode );
        demoButton.setChecked( DataLayer.get().isDemo() );
        demoButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                boolean on = ((ToggleButton) v).isChecked();
                DataLayer.get().setDemoMode( on );
            }
        } );

        return layout;
    }
}
