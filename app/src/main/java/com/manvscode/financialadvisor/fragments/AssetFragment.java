package com.manvscode.financialadvisor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.Asset;
import com.manvscode.wealth.AssetClass;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.Profile;


public class AssetFragment extends DialogFragment
{
    private static final String TAG        = "AssetFragment";
    private static final String ARG_HANDLE = "handle";
    protected Asset mAsset           = null;
    protected String mDescription    = "";
    protected double mAmount         = 0.0;
    protected AssetClass mAssetClass = AssetClass.Unspecified;



    public static AssetFragment newInstance( Asset asset )
    {
        AssetFragment fragment = new AssetFragment( );
        Bundle args = new Bundle( );
        args.putLong( ARG_HANDLE, asset.handle() );
        fragment.setArguments( args );
        return fragment;
    }

    public static AssetFragment newInstance( )
    {
        AssetFragment fragment = new AssetFragment( );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if( getArguments( ) != null )
        {
            long handle = getArguments().getLong( ARG_HANDLE );

            if( handle == 0L )
            {
                mAsset       = null;
                mDescription = "";
                mAmount      = 0.0;
                mAssetClass  = AssetClass.Unspecified;
            }
            else
            {
                mAsset       = new Asset( handle );
                mDescription = mAsset.description();
                mAmount      = mAsset.amount();
                mAssetClass  = mAsset.assetClass();
            }
        }
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        String title = isEditing() ? getString( R.string.update_asset ) : getString( R.string.create_asset );
        Dialog dlg = getDialog();

        if( dlg != null )
        {
            dlg.setTitle( title );
            dlg.setCancelable( true );
            //int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
            //int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
            dlg.getWindow().setLayout( Utils.convertDipsToPixels( 200 ), Utils.convertDipsToPixels( 200 ) );
        }

        View layout = inflater.inflate( R.layout.fragment_asset, container, false );

        final EditText descriptionEditText = (EditText) layout.findViewById( R.id.asset_description_edit_text );
        final EditText amountEditText = (EditText) layout.findViewById( R.id.asset_amount_edit_text );

        String[] assetClassArray = getResources().getStringArray( R.array.asset_classes );
        final Spinner assetClassSpinner = (Spinner) layout.findViewById( R.id.asset_class_spinner );
        assetClassSpinner.setAdapter( new ArrayAdapter<String>( getActivity(), android.R.layout.simple_dropdown_item_1line, assetClassArray ) );
        assetClassSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
            {
                mAssetClass = AssetClass.values()[ position ];
            }

            @Override
            public void onNothingSelected( AdapterView<?> parent )
            {
            }
        } );

        if( isEditing() )
        {
            descriptionEditText.setText( mDescription );
            amountEditText.setText( Double.toString( mAmount ) );
            assetClassSpinner.setSelection( mAssetClass.getValue() );
        }

        Button saveButton = (Button) layout.findViewById( R.id.asset_done_button );
        saveButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Profile profile = DataLayer.get().getProfile();
                mDescription = descriptionEditText.getText().toString();
                try
                {
                    mAmount = Double.parseDouble( amountEditText.getText().toString() );
                }
                catch( Exception e )
                {
                    mAmount = 0.0;
                }

                if( isEditing() )
                {
                    mAsset.setDescription( mDescription );
                    mAsset.setAmount( mAmount );
                    mAsset.setAssetClass( mAssetClass );

                    Toast.makeText( getActivity(), getString( R.string.toast_asset_saved), Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    Asset asset = (Asset) profile.addItem( ItemType.Asset, mDescription, mAmount );
                    asset.setAssetClass( mAssetClass );
                    Toast.makeText( getActivity(), getString( R.string.toast_asset_created), Toast.LENGTH_SHORT ).show();
                }

                profile.refresh();

                close();
            }
        } );

        Button cancelButton = (Button) layout.findViewById( R.id.asset_cancel_button );
        cancelButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                close();
            }
        } );
        return layout;
    }

    public void close( )
    {
        Dialog dlg = getDialog();

        if( dlg == null )
        {
            getActivity().finish();
        }
        else
        {
            AssetFragment.this.dismiss();
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Log.i( TAG, "Asset fragment started." );
    }

    public boolean isEditing()
    {
        return mAsset != null;
    }

}
