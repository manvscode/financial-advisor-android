package com.manvscode.financialadvisor.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.manvscode.financialadvisor.R;


public class AboutFragment extends Fragment
{
    public static AboutFragment newInstance( )
    {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    public AboutFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_about, container, false );
    }
}
