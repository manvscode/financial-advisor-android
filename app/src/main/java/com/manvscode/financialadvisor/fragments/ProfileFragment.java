package com.manvscode.financialadvisor.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.manvscode.financialadvisor.FontUtil;
import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.Asset;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.Profile;

import org.achartengine.GraphicalView;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment
{
    private static final String TAG = "ProfileFragment";

    protected View mLayout;
    protected TextView mStatus;
    protected TextView mNetWorthValue;
    protected TextView mGoalValue;
    TextView mProgessValue;
    TextView mTotalAssetsValue;
    TextView mTotalLiabilitiesValue;
    TextView mDebtToIncomeRatio;
    TextView mCreditScoreValue;
    TextView mCreditScoreLastUpdated;
    TextView mMonthlyIncomeValue;
    TextView mMonthlyExpensesValue;
    TextView mMonthlyExpensesToIncomeValue;
    TextView mDisposableIncomeValue;

    private OnFragmentInteractionListener mListener;
    private GraphicalView mLongtimeTotalsChart;
    private GraphicalView mMonthlyTotalsChart;



    public static ProfileFragment newInstance(  )
    {
        ProfileFragment fragment = new ProfileFragment( );
        return fragment;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        mLayout = inflater.inflate( R.layout.fragment_profile, container, false );

        mStatus = (TextView) mLayout.findViewById( R.id.profile_status );
        mNetWorthValue = (TextView) mLayout.findViewById( R.id.profile_net_worth_value );
        mGoalValue = (TextView) mLayout.findViewById( R.id.profile_goal_value );
        mProgessValue = (TextView) mLayout.findViewById( R.id.profile_progress_value );
        mTotalAssetsValue = (TextView) mLayout.findViewById( R.id.profile_total_assets_value );
        mTotalLiabilitiesValue = (TextView) mLayout.findViewById( R.id.profile_total_liabilities_value );
        mDebtToIncomeRatio = (TextView) mLayout.findViewById( R.id.profile_debt_to_income_value );
        mCreditScoreValue = (TextView) mLayout.findViewById( R.id.profile_credit_score );
        mCreditScoreLastUpdated = (TextView) mLayout.findViewById( R.id.profile_credit_score_last_updated );
        mMonthlyIncomeValue = (TextView) mLayout.findViewById( R.id.profile_monthly_income_value );
        mMonthlyExpensesValue = (TextView) mLayout.findViewById( R.id.profile_monthly_expenses_value );
        mMonthlyExpensesToIncomeValue = (TextView) mLayout.findViewById( R.id.profile_monthly_expenses_to_income_value );
        mDisposableIncomeValue = (TextView) mLayout.findViewById( R.id.profile_disposable_income_value );

        refresh();

        return mLayout;
    }

    protected void createLongtermTotalsChart( View parent, Profile profile )
    {
        ArrayList<Integer> colorsList = new ArrayList<Integer>();
        ArrayList<String> namesList   = new ArrayList<String>();
        ArrayList<Double> valuesList  = new ArrayList<Double>();

        if( profile.totalAssets() > 0.0 )
        {
            colorsList.add( new Integer( getResources().getColor(R.color.positive_value) ) );
            namesList.add( getString( R.string.total_assets ) );
            valuesList.add( new Double(profile.totalAssets()) );
        }
        if( profile.totalLiabilities() > 0.0 )
        {
            colorsList.add( new Integer( getResources().getColor(R.color.negative_value) ) );
            namesList.add( getString( R.string.total_liabilities ) );
            valuesList.add( new Double(profile.totalLiabilities()) );
        }

        int[] colors    = new int[ colorsList.size() ];
        String[] names  = namesList.toArray( new String[ namesList.size() ] );
        double[] values = new double[ valuesList.size() ];

        for( int i = 0; i < colorsList.size(); i++ ) colors[ i ] = colorsList.get( i ).intValue();
        for( int i = 0; i < valuesList.size(); i++ ) values[ i ] = valuesList.get( i ).doubleValue();

//        int[] colors = new int[]{getResources().getColor( R.color.positive_value ), getResources().getColor( R.color.negative_value )};
//        double[] values = new double[]{profile.profileTotalAssets(), profile.profileTotalLiabilities()};
//        String[] names = new String[]{getString( R.string.total_assets ), getString( R.string.total_liabilities )};

        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsColor( getResources().getColor( R.color.soft_black ) );
        renderer.setApplyBackgroundColor( false );
        renderer.setChartTitleTextSize( Utils.convertDipsToPixels( getActivity(), 28 ) );
        renderer.setLabelsTextSize( Utils.convertDipsToPixels( getActivity(), 12 ) );
        renderer.setLegendTextSize( Utils.convertDipsToPixels( getActivity(), 12 ) );
        renderer.setAntialiasing( true );
        renderer.setZoomButtonsVisible( false );
        renderer.setStartAngle( 90 );
        renderer.setExternalZoomEnabled( false );
        renderer.setClickEnabled( false );
        renderer.setSelectableBuffer( 10 );
        renderer.setPanEnabled( false );
        renderer.setShowLegend( false );
        renderer.setInScroll( true );
        mLongtimeTotalsChart = Utils.createPieChart( getActivity(), names, values, colors, renderer );

        mLongtimeTotalsChart.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                SeriesSelection seriesSelection = mLongtimeTotalsChart.getCurrentSeriesAndPoint();

                if( seriesSelection == null )
                {
                    Toast.makeText( getActivity(), "No chart element was clicked", Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    Toast.makeText( getActivity(), "Chart element data point index " + (seriesSelection.getPointIndex() + 1) + " was clicked" + " point value=" + seriesSelection.getValue(), Toast.LENGTH_SHORT ).show();
                }
            }
        } );

        mLongtimeTotalsChart.setOnLongClickListener( new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick( View v )
            {
                SeriesSelection seriesSelection = mLongtimeTotalsChart.getCurrentSeriesAndPoint();
                if( seriesSelection == null )
                {
                    Toast.makeText( getActivity(), "No chart element was long pressed", Toast.LENGTH_SHORT );
                    return false;
                }
                else
                {
                    Toast.makeText( getActivity(), "Chart element data point index " + seriesSelection.getPointIndex() + " was long pressed", Toast.LENGTH_SHORT );
                    return true;
                }
            }
        } );

        LinearLayout layout = (LinearLayout) parent.findViewById( R.id.profile_longterm_totals_chart );
        layout.removeAllViews();
        layout.addView( mLongtimeTotalsChart, new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ) );

        double sum = profile.totalAssets() + profile.totalLiabilities();

        if( sum > 0.0 )
        {
            layout.setVisibility( View.VISIBLE );
        }
        else
        {
            layout.setVisibility( View.GONE );
        }
    }

    protected void createMonthlyTotalsChart( View parent, Profile profile )
    {
        ArrayList<Integer> colorsList = new ArrayList<Integer>();
        ArrayList<String> namesList  = new ArrayList<String>();
        ArrayList<Double> valuesList  = new ArrayList<Double>();

        if( profile.disposableIncome() > 0.0 )
        {
            colorsList.add( new Integer( getResources().getColor(R.color.positive_value) ) );
            namesList.add( getString( R.string.disposable_income ) );
            valuesList.add( new Double(profile.disposableIncome()) );
        }
        if( profile.totalMonthlyExpenses() > 0.0 )
        {
            colorsList.add( new Integer( getResources().getColor(R.color.negative_value) ) );
            namesList.add( getString( R.string.total_monthly_expenses ) );
            valuesList.add( new Double(profile.totalMonthlyExpenses()) );
        }

        int[] colors    = new int[ colorsList.size() ];
        String[] names  = namesList.toArray( new String[ namesList.size() ] );
        double[] values = new double[ valuesList.size() ];

        for( int i = 0; i < colorsList.size(); i++ ) colors[ i ] = colorsList.get( i ).intValue();
        for( int i = 0; i < valuesList.size(); i++ ) values[ i ] = valuesList.get( i ).doubleValue();
//        int[] colors    = new int[] { getResources().getColor(R.color.positive_value ), getResources().getColor(R.color.negative_value ) };
//        double[] values = new double[] { profile.profileDisposableIncome(), profile.profileTotalMonthlyExpenses() };
//        String[] names  = new String[] {  getString( R.string.disposable_income ), getString( R.string.total_monthly_expenses ) };

        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsColor( getResources( ).getColor( R.color.soft_black ) );
        renderer.setApplyBackgroundColor( false );
        renderer.setChartTitleTextSize( Utils.convertDipsToPixels( getActivity( ), 28 ) );
        renderer.setLabelsTextSize( Utils.convertDipsToPixels( getActivity( ), 12 ) );
        renderer.setLegendTextSize( Utils.convertDipsToPixels( getActivity( ), 12 ) );
        renderer.setAntialiasing( true );
        renderer.setZoomButtonsVisible( false );
        renderer.setStartAngle( 0 );
        renderer.setExternalZoomEnabled( false );
        renderer.setClickEnabled( false );
        renderer.setSelectableBuffer( 10 );
        renderer.setPanEnabled( false );
        renderer.setShowLegend( false );
        renderer.setInScroll( true );
        mMonthlyTotalsChart = Utils.createPieChart( getActivity(), names, values, colors, renderer );

        mMonthlyTotalsChart.setOnClickListener( new View.OnClickListener( )
        {
            @Override
            public void onClick( View v )
            {
                SeriesSelection seriesSelection = mMonthlyTotalsChart.getCurrentSeriesAndPoint( );

                if( seriesSelection == null )
                {
                    Toast.makeText( getActivity( ), "No chart element was clicked", Toast.LENGTH_SHORT ).show( );
                }
                else
                {
                    Toast.makeText( getActivity( ), "Chart element data point index " + (seriesSelection.getPointIndex( ) + 1) + " was clicked" + " point value=" + seriesSelection.getValue( ), Toast.LENGTH_SHORT ).show( );
                }
            }
        } );

        mMonthlyTotalsChart.setOnLongClickListener( new View.OnLongClickListener( )
        {
            @Override
            public boolean onLongClick( View v )
            {
                SeriesSelection seriesSelection = mMonthlyTotalsChart.getCurrentSeriesAndPoint( );
                if( seriesSelection == null )
                {
                    Toast.makeText( getActivity( ), "No chart element was long pressed", Toast.LENGTH_SHORT );
                    return false;
                }
                else
                {
                    Toast.makeText( getActivity( ), "Chart element data point index " + seriesSelection.getPointIndex( ) + " was long pressed", Toast.LENGTH_SHORT );
                    return true;
                }
            }
        } );

        LinearLayout layout = (LinearLayout) parent.findViewById( R.id.profile_monthly_totals_chart );
        layout.removeAllViews();
        layout.addView( mMonthlyTotalsChart, new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        double sum = profile.disposableIncome() + profile.totalMonthlyExpenses();

        if( sum > 0.0 )
        {
            layout.setVisibility( View.VISIBLE );
        }
        else
        {
            layout.setVisibility( View.GONE );
        }
    }


    @Override
    public void onResume()
    {
        super.onResume();
        refresh();
    }

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );
//        try
//        {
//            mListener = (OnFragmentInteractionListener) activity;
//        }
//        catch( ClassCastException e )
//        {
//            throw new ClassCastException( activity.toString( )
//                    + " must implement OnFragmentInteractionListener" );
//        }
    }

    @Override
    public void onDetach( )
    {
        super.onDetach( );
        mListener = null;
    }

    public void refresh()
    {
        final Profile profile = DataLayer.get().getProfile();

        mStatus.setText( String.format( "%sK to %s", Utils.formatCurrency( profile.toGoal() / 100000, 2 ), "Millionaire" ) );
        mNetWorthValue.setText( Utils.formatCurrency( profile.netWorth(), 0 ) );
        mGoalValue.setText( Utils.formatCurrency( profile.goal(), 0 ) );
        mProgessValue.setText( String.format( getString( R.string.percent_of_goal ), Utils.formatPercentage( profile.progress(), 0 ) ) );
        mTotalAssetsValue.setText( Utils.formatCurrency( profile.totalAssets(), 2 ) );
        mTotalLiabilitiesValue.setText( Utils.formatCurrency( profile.totalLiabilities(), 2 ) );
        mDebtToIncomeRatio.setText( Utils.formatPercentage( profile.debtToIncomeRatio(), 1 ) );

        if( profile.creditScore() >= 0 )
        {
            mCreditScoreValue.setText( Integer.toString( profile.creditScore() ) );

            int ts = profile.creditScoreLastUpdate( );
            if( ts > 0 )
            {
                mCreditScoreLastUpdated.setText( String.format( getString( R.string.as_of_date ), Utils.dateShortString( ts ) ) );
                mCreditScoreLastUpdated.setVisibility( View.VISIBLE );
            }
            else
            {
                mCreditScoreLastUpdated.setVisibility( View.GONE );
            }
        }

        mMonthlyIncomeValue.setText( Utils.formatCurrency( profile.monthlyIncome(), 2 ) );
        mMonthlyExpensesValue.setText( Utils.formatCurrency( profile.totalMonthlyExpenses(), 2 ) );


        if( profile.monthlyIncome() > 0.0 )
        {
            mMonthlyExpensesToIncomeValue.setText( String.format( getString( R.string.percent_of_income ), Utils.formatPercentage( profile.totalMonthlyExpenses() / profile.monthlyIncome(), 0 ) ) );
            mMonthlyExpensesToIncomeValue.setVisibility( View.VISIBLE );
        }
        else
        {
            mMonthlyExpensesToIncomeValue.setVisibility( View.GONE );
        }

        mDisposableIncomeValue.setText( Utils.formatCurrency( profile.disposableIncome(), 2 ) );

        createLongtermTotalsChart( mLayout, profile );
        createMonthlyTotalsChart( mLayout, profile );
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction( Uri uri );
    }

}
