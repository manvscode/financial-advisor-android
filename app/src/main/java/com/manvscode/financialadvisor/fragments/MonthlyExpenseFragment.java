package com.manvscode.financialadvisor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.MonthlyExpense;
import com.manvscode.wealth.Profile;


public class MonthlyExpenseFragment extends DialogFragment
{
    private static final String TAG        = "MonthlyExpenseFragment";
    private static final String ARG_HANDLE = "handle";
    protected MonthlyExpense mMonthlyExpense   = null;
    protected String mDescription    = "";
    protected double mAmount         = 0.0;



    public static MonthlyExpenseFragment newInstance( MonthlyExpense item )
    {
        MonthlyExpenseFragment fragment = new MonthlyExpenseFragment( );
        Bundle args = new Bundle( );
        args.putLong( ARG_HANDLE, item.handle() );
        fragment.setArguments( args );
        return fragment;
    }

    public static MonthlyExpenseFragment newInstance( )
    {
        MonthlyExpenseFragment fragment = new MonthlyExpenseFragment( );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if( getArguments( ) != null )
        {
            long handle = getArguments().getLong( ARG_HANDLE );

            if( handle == 0L )
            {
                mMonthlyExpense      = null;
                mDescription    = "";
                mAmount         = 0.0;
            }
            else
            {
                mMonthlyExpense = new MonthlyExpense( handle );
                mDescription    = mMonthlyExpense.description();
                mAmount         = mMonthlyExpense.amount();
            }
        }
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        String title = isEditing() ? getString( R.string.update_expense ) : getString( R.string.create_expense );
        Dialog dlg = getDialog();

        if( dlg != null )
        {
            dlg.setTitle( title );
            dlg.setCancelable( true );
            //int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
            //int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
            dlg.getWindow().setLayout( Utils.convertDipsToPixels( 200 ), Utils.convertDipsToPixels( 200 ) );
        }

        View layout = inflater.inflate( R.layout.fragment_monthly_expense, container, false );

        final EditText descriptionEditText = (EditText) layout.findViewById( R.id.expense_description_edit_text );
        final EditText amountEditText = (EditText) layout.findViewById( R.id.expense_amount_edit_text );

        if( isEditing() )
        {
            descriptionEditText.setText( mDescription );
            amountEditText.setText( Double.toString( mAmount ) );
        }

        Button saveButton = (Button) layout.findViewById( R.id.expense_done_button );
        saveButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Profile profile = DataLayer.get().getProfile();
                mDescription = descriptionEditText.getText().toString();
                try
                {
                    mAmount = Double.parseDouble( amountEditText.getText().toString() );
                }
                catch( Exception e )
                {
                    mAmount = 0.0;
                }

                if( isEditing() )
                {
                    mMonthlyExpense.setDescription( mDescription );
                    mMonthlyExpense.setAmount( mAmount );

                    Toast.makeText( getActivity(), getString( R.string.toast_expense_saved), Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    MonthlyExpense item = (MonthlyExpense) profile.addItem( ItemType.MonthlyExpense, mDescription, mAmount );
                    Toast.makeText( getActivity(), getString( R.string.toast_expense_created), Toast.LENGTH_SHORT ).show();
                }

                profile.refresh();

                close();
            }
        } );

        Button cancelButton = (Button) layout.findViewById( R.id.expense_cancel_button );
        cancelButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                close();
            }
        } );
        return layout;
    }

    public void close( )
    {
        Dialog dlg = getDialog();

        if( dlg == null )
        {
            getActivity().finish();
        }
        else
        {
            MonthlyExpenseFragment.this.dismiss();
        }
    }

    public boolean isEditing()
    {
        return mMonthlyExpense != null;
    }

}
