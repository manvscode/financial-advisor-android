package com.manvscode.financialadvisor.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.adapters.ItemsAdapter;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.ItemSort;
import com.manvscode.wealth.Profile;


public abstract class ItemsFragment extends Fragment
{
    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setHasOptionsMenu( true );
    }

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater)
    {
        inflater.inflate( R.menu.item, menu);
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        Profile profile = DataLayer.get().getProfile();

        switch( id )
        {
            case R.id.action_sort_description_asc:
            {
                profile.sort( ItemSort.DescriptionAscending );
                getAdapter().notifyDataSetChanged();

                Toast.makeText( getActivity(), getString( R.string.toast_sort_description_asc ), Toast.LENGTH_SHORT ).show();
                return true;
            }
            case R.id.action_sort_description_des:
            {
                profile.sort( ItemSort.DescriptionDescending );
                getAdapter().notifyDataSetChanged();

                Toast.makeText( getActivity(), getString( R.string.toast_sort_description_des ), Toast.LENGTH_SHORT ).show();
                return true;
            }
            case R.id.action_sort_amount_asc:
            {
                profile.sort( ItemSort.AmountAscending );
                getAdapter().notifyDataSetChanged();

                Toast.makeText( getActivity(), getString( R.string.toast_sort_amount_asc ), Toast.LENGTH_SHORT ).show();
                return true;
            }
            case R.id.action_sort_amount_des:
            {
                profile.sort( ItemSort.AmountDescending );
                getAdapter().notifyDataSetChanged();

                Toast.makeText( getActivity(), getString( R.string.toast_sort_amount_des ), Toast.LENGTH_SHORT ).show();
                return true;
            }
            case R.id.action_settings:
            {
                return true;
            }
            default:
            {

                return super.onOptionsItemSelected(item);
            }
        }
    }

    public abstract ItemsAdapter getAdapter();
}
