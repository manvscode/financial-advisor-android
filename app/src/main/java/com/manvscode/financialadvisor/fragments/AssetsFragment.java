//-------------------------------------------------------------
// Copyright (C) 2015 Joseph A. Marrero.  All rights reserved.
// http://www.manvscode.com/
//-------------------------------------------------------------
package com.manvscode.financialadvisor.fragments;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.Utils;
import com.manvscode.financialadvisor.activities.AssetActivity;
import com.manvscode.financialadvisor.adapters.AssetsAdapter;
import com.manvscode.financialadvisor.adapters.ItemsAdapter;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.wealth.Asset;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.Profile;

import org.achartengine.GraphicalView;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;

import java.util.Map;
import java.util.TreeMap;


public class AssetsFragment extends ItemsFragment implements AssetsAdapter.OnAssetEditListener
{
    private static final String TAG = "AssetsFragment";
    protected View mLayout;
    protected ListView mListView;
    protected AssetsAdapter mAdapter;
    protected GraphicalView mAssetChart;

    public static AssetsFragment newInstance(  )
    {
        AssetsFragment fragment = new AssetsFragment( );
        return fragment;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        final Profile profile = DataLayer.get().getProfile();
        // Inflate the layout for this fragment
        mLayout = inflater.inflate( R.layout.fragment_assets, container, false );

        this.createAssetChart( mLayout, profile );

        mAdapter = new AssetsAdapter( getActivity(), profile, this );
        mListView = (ListView) mLayout.findViewById( R.id.assets_listview );
        mListView.setAdapter( mAdapter );

        Button addButton = (Button) mLayout.findViewById( R.id.assets_add_button );
        addButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent intent = new Intent( getActivity(), AssetActivity.class );
                startActivityForResult( intent, AssetActivity.REQUEST_ASSET_CHANGE );
            }
        } );

        Button editButton = (Button) mLayout.findViewById( R.id.assets_edit_button );
        editButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mAdapter.toggleEditMode();
            }
        } );

        return mLayout;
    }

    public void onAssetEdit( Asset a )
    {
        Intent intent = new Intent( getActivity(), AssetActivity.class );
        intent.putExtra( "handle", a.handle() );
        startActivityForResult( intent, AssetActivity.REQUEST_ASSET_CHANGE );
    }

    public void onAssetRemoved()
    {
        refresh();
    }



    protected void createAssetChart( View parent, Profile profile )
    {
        TypedArray chart_colors = getResources().obtainTypedArray(R.array.chart_colors);

        int[] colors    = new int[chart_colors.length()];
        for( int i = 0; i < colors.length; i++ ) colors[i] = chart_colors.getColor( i, 0 );

        Map<String, Double> chartMap = new TreeMap<String,Double>();
        for( int i = 0; i < profile.countItems( ItemType.Asset ); i++ )
        {
            Asset asset = (Asset) profile.getItem( ItemType.Asset, (long) i );
            String key = Utils.assetClassString( getActivity(), asset.assetClass() );
            double val = asset.amount();

            if( val > 0.0 )
            {
                if( chartMap.containsKey( key ) )
                {
                    Double sum = chartMap.get( key );
                    chartMap.put( key, new Double(sum.doubleValue() + val) );
                }
                else
                {
                    chartMap.put( key, new Double(val) );
                }
            }
        }

        String[] names  = chartMap.keySet().toArray( new String[ chartMap.size() ] );
        Double[] values = chartMap.values().toArray( new Double[ chartMap.size() ] );
        double[] sums   = new double[ values.length ];
        for(int i = 0; i < values.length; i++ ) sums[ i ] = values[ i ].doubleValue();

        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsColor( getResources( ).getColor( R.color.soft_black ) );
        renderer.setApplyBackgroundColor( false );
        renderer.setChartTitleTextSize( Utils.convertDipsToPixels( getActivity( ), 28 ) );
        renderer.setLabelsTextSize( Utils.convertDipsToPixels( getActivity( ), 12 ) );
        renderer.setLegendTextSize( Utils.convertDipsToPixels( getActivity( ), 12 ) );
        renderer.setAntialiasing( true );
        renderer.setZoomButtonsVisible( false );
        renderer.setStartAngle( 0 );
        renderer.setExternalZoomEnabled( false );
        renderer.setClickEnabled( false );
        renderer.setSelectableBuffer( 10 );
        renderer.setPanEnabled( false );
        renderer.setShowLegend( false );
        renderer.setInScroll( true );
        mAssetChart = Utils.createPieChart( getActivity(), names, sums, colors, renderer );

        mAssetChart.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                SeriesSelection seriesSelection = mAssetChart.getCurrentSeriesAndPoint();

                if( seriesSelection == null )
                {
                    Toast.makeText( getActivity(), "No chart element was clicked", Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    Toast.makeText( getActivity(), "Chart element data point index " + (seriesSelection.getPointIndex() + 1) + " was clicked" + " point value=" + seriesSelection.getValue(), Toast.LENGTH_SHORT ).show();
                }
            }
        } );

        mAssetChart.setOnLongClickListener( new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick( View v )
            {
                SeriesSelection seriesSelection = mAssetChart.getCurrentSeriesAndPoint();
                if( seriesSelection == null )
                {
                    Toast.makeText( getActivity(), "No chart element was long pressed", Toast.LENGTH_SHORT );
                    return false;
                }
                else
                {
                    Toast.makeText( getActivity(), "Chart element data point index " + seriesSelection.getPointIndex() + " was long pressed", Toast.LENGTH_SHORT );
                    return true;
                }
            }
        } );

        LinearLayout layout = (LinearLayout) parent.findViewById( R.id.assets_chart );
        layout.removeAllViews();
        layout.addView( mAssetChart, new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if( requestCode == AssetActivity.REQUEST_ASSET_CHANGE )
        {
            refresh();
        }
    }

    public void refresh()
    {
        final Profile profile = DataLayer.get().getProfile();
        this.createAssetChart( mLayout, profile );
        mAdapter.notifyDataSetChanged();
    }

    public ItemsAdapter getAdapter()
    {
        return mAdapter;
    }
}
