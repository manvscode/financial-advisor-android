package com.manvscode.financialadvisor.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.activities.HelpTopicActivity;

import org.w3c.dom.Text;

public class HelpListFragment extends Fragment
{

    public static HelpListFragment newInstance(  )
    {
        HelpListFragment fragment = new HelpListFragment( );
        return fragment;
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        View layout = inflater.inflate( R.layout.fragment_help_list, container, false );
        LinearLayout linearLayout = (LinearLayout) layout.findViewById( R.id.help_list );
        int count = linearLayout.getChildCount();

        for( int i=0; i < count; i++ )
        {
            TextView tv = (TextView) linearLayout.getChildAt( i );

            tv.setOnClickListener( new View.OnClickListener()
            {
                @Override
                public void onClick( View v )
                {
                    TextView textView = (TextView) v;
                    String topic = textView.getText().toString();

                    String file = String.format( "%s", topic.toLowerCase().replace( ' ', '-' ) );

                    Intent i = new Intent( getActivity(), HelpTopicActivity.class );
                    i.putExtra( "title", topic );
                    i.putExtra( "file", file );
                    startActivity( i );
                }
            } );
        }

        return layout;
    }
}
