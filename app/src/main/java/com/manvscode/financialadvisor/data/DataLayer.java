package com.manvscode.financialadvisor.data;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.manvscode.financialadvisor.FinancialAdvisorApp;
import com.manvscode.wealth.Profile;

import java.io.File;

public class DataLayer
{
    protected static final String TAG = "DataLayer";
    protected static DataLayer mInstance = null;
    protected Context mContext = null;
    protected Profile mProfile = null;
    protected boolean mIsDemo;


    public static DataLayer get()
    {
        if( mInstance == null )
        {
            mInstance = new DataLayer();
        }

        return mInstance;
    }

    protected DataLayer()
    {
        mContext = FinancialAdvisorApp.get( );
    }



    public String getDataDirectory( )
    {
        String result      = null;
        PackageManager pm  = mContext.getPackageManager();
        String packageName = mContext.getPackageName();

        try
        {
            PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
            result = packageInfo.applicationInfo.dataDir;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.w( TAG, "Error Package name not found ", e );
        }

        return result;
    }

    public Profile getProfile()
    {
        if( mProfile == null )
        {
            load();
        }

        return mProfile;
    }

    protected Profile loadProfile( )
    {
        Profile profile = null;
        //String dataDir = getDataDirectory();
        File dataDir = mContext.getFilesDir();

        if( dataDir != null )
        {
            File file = new File( dataDir, "profile.fp" );

            if( file.exists() )
            {
                profile = Profile.load( file.getPath( ) );
            }
            else
            {
                Log.w(TAG, "Profile data file does not exist!");
                profile = Profile.newInstance();
            }
        }

        return profile;
    }

    protected boolean saveProfile( Profile profile )
    {
        boolean result = false;
        //String dataDir = getDataDirectory();
        File dataDir = mContext.getFilesDir( );

        if( dataDir != null )
        {
            File file = new File(dataDir, "profile.fp");
            result = profile.save( file.getPath() );
        }

        return result;
    }

    public void setDemoMode( boolean isOn )
    {
        if( isOn )
        {
            // Save core profile.
            save();
            mProfile = Profile.demo();
        }
        else
        {
            // Restore core profile.
            load();
        }

        mIsDemo = isOn;
    }

    public boolean isDemo()
    {
        return mIsDemo;
    }

    public boolean load( )
    {
        mProfile = loadProfile();
        return mProfile != null;
    }

    public boolean save( )
    {
        return saveProfile( mProfile );
    }
}
