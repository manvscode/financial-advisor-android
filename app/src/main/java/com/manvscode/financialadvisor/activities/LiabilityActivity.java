package com.manvscode.financialadvisor.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.fragments.LiabilityFragment;
import com.manvscode.wealth.Liability;

public class LiabilityActivity extends ActionBarActivity
{
    public static final int REQUEST_LIABILITY_CHANGE = 2;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_one_fragment );

        LiabilityFragment fragment = null;

        Intent i = getIntent();
        long handle = i.getLongExtra( "handle", 0L );

        if( handle != 0L )
        {
            setTitle( getString( R.string.update_liability ) );
            Liability item = new Liability( handle );
            fragment = LiabilityFragment.newInstance( item );
        }
        else
        {
            setTitle( getString( R.string.create_liability ) );
            fragment = LiabilityFragment.newInstance( );
        }


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace( R.id.container, fragment );
        transaction.commit();
    }
}
