package com.manvscode.financialadvisor.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.fragments.ProfileFragment;

public class ProfileActivity extends FragmentActivity
{
    public ProfileActivity()
    {

    }

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.fragment_profile );

        FragmentManager manager = getSupportFragmentManager();

        ProfileFragment profileFragment = ProfileFragment.newInstance();
        FragmentTransaction t = manager.beginTransaction();
        t.replace( 0, profileFragment );
        t.commit();
    }

}
