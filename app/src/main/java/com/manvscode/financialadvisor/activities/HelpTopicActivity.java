package com.manvscode.financialadvisor.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.manvscode.financialadvisor.R;

import java.io.File;

public class HelpTopicActivity extends ActionBarActivity
{
    protected WebView mWebView;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_help_topic );

        Intent i        = getIntent();
        String title    = i.getStringExtra( "title" );
        String filename = i.getStringExtra( "file" );
        String url      = String.format( "file:///android_asset/help/%s.html", filename );

        //this.setTitle( title );
        this.setTitle( getString( R.string.title_help ) );

        mWebView = (WebView) this.findViewById( R.id.help_topic_web_view );
        mWebView.getSettings().setJavaScriptEnabled( true );
        mWebView.getSettings().setSaveFormData( true );
        mWebView.getSettings().setBuiltInZoomControls( false );
        mWebView.loadUrl( url );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_help_topic, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if( id == R.id.action_settings )
        {
            return true;
        }

        return super.onOptionsItemSelected( item );
    }
}
