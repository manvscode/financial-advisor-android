package com.manvscode.financialadvisor.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.manvscode.financialadvisor.BuildConfig;
import com.manvscode.financialadvisor.R;

public class SplashScreenActivity extends Activity
{

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_splash);

        int timeOut = BuildConfig.DEBUG ? 1 : SPLASH_TIME_OUT;

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run()
            {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent( SplashScreenActivity.this, MainActivity.class );
                startActivity( i );

                // close this activity
                finish( );
            }
        }, timeOut);
    }

}