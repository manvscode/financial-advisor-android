package com.manvscode.financialadvisor.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.fragments.AssetFragment;
import com.manvscode.wealth.Asset;

public class AssetActivity extends ActionBarActivity
{
    public static final int REQUEST_ASSET_CHANGE = 1;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_one_fragment );

        AssetFragment fragment = null;

        Intent i = getIntent();
        long handle = i.getLongExtra( "handle", 0L );

        if( handle != 0L )
        {
            setTitle( getString( R.string.update_asset ));
            Asset asset = new Asset( handle );
            fragment = AssetFragment.newInstance( asset );
        }
        else
        {
            setTitle( getString( R.string.create_asset ) );
            fragment = AssetFragment.newInstance( );
        }


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace( R.id.container, fragment );
        transaction.commit();
    }
}
