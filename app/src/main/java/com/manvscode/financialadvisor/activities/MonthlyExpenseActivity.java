package com.manvscode.financialadvisor.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.fragments.MonthlyExpenseFragment;
import com.manvscode.wealth.MonthlyExpense;

public class MonthlyExpenseActivity extends ActionBarActivity
{
    public static final int REQUEST_EXPENSE_CHANGE = 3;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_one_fragment );

        MonthlyExpenseFragment fragment = null;

        Intent i = getIntent();
        long handle = i.getLongExtra( "handle", 0L );

        if( handle != 0L )
        {
            setTitle( getString( R.string.update_expense ));
            MonthlyExpense item = new MonthlyExpense( handle );
            fragment = MonthlyExpenseFragment.newInstance( item );
        }
        else
        {
            setTitle( getString( R.string.create_expense ) );
            fragment = MonthlyExpenseFragment.newInstance( );
        }


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace( R.id.container, fragment );
        transaction.commit();
    }
}
