package com.manvscode.financialadvisor.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.TextView;

import com.manvscode.financialadvisor.FontUtil;
import com.manvscode.financialadvisor.data.DataLayer;
import com.manvscode.financialadvisor.R;
import com.manvscode.financialadvisor.fragments.AboutFragment;
import com.manvscode.financialadvisor.fragments.AssetsFragment;
import com.manvscode.financialadvisor.fragments.HelpListFragment;
import com.manvscode.financialadvisor.fragments.LiabilitiesFragment;
import com.manvscode.financialadvisor.fragments.MonthlyExpensesFragment;
import com.manvscode.financialadvisor.fragments.NavigationDrawerFragment;
import com.manvscode.financialadvisor.fragments.ProfileFragment;
import com.manvscode.financialadvisor.fragments.SettingsFragment;
import com.manvscode.wealth.ItemType;
import com.manvscode.wealth.Profile;


public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private static final String TAG = "MainActivity";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main);

        Profile profile = DataLayer.get( ).getProfile();

        Log.d( TAG, "          # of Assets: " + profile.countItems( ItemType.Asset ) );
        Log.d( TAG, "     # of Liabilities: " + profile.countItems( ItemType.Liability ) );
        Log.d( TAG, "# of Monthly Expenses: " + profile.countItems( ItemType.MonthlyExpense ) );


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onPause( )
    {
        Log.d( TAG, "MainActivity is paused." );
        DataLayer dataLayer = DataLayer.get( );
        dataLayer.save();

        super.onPause( );
    }

    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        switch( position )
        {
            case 1:
                mTitle = getString(R.string.title_assets);
                transaction.replace( R.id.container, AssetsFragment.newInstance() );
                break;
            case 2:
                mTitle = getString(R.string.title_liabilities);
                transaction.replace( R.id.container, LiabilitiesFragment.newInstance() );
                break;
            case 3:
                mTitle = getString(R.string.title_monthly_expenses);
                transaction.replace( R.id.container, MonthlyExpensesFragment.newInstance() );
                break;
            case 4:
                mTitle = getString(R.string.title_settings);
                transaction.replace( R.id.container, SettingsFragment.newInstance());
                break;
            //case 5:
            //    mTitle = getString(R.string.title_help);
                //transaction.replace( R.id.container, HelpListFragment.newInstance() );
                //break;
            //case 6:
            case 5:
                mTitle = getString(R.string.title_about);
                transaction.replace( R.id.container, AboutFragment.newInstance() );
                break;
            case 0:
            default:
                mTitle = getString(R.string.title_financial_profile);
                transaction.replace( R.id.container, ProfileFragment.newInstance() );
                break;
        }

        transaction.commit( );
    }

    public void restoreActionBar()
    {
        if( false )
        {
            LayoutInflater inflater = (LayoutInflater) getSystemService( LAYOUT_INFLATER_SERVICE );
            View actionBarLayout = inflater.inflate( R.layout.action_bar_layout, null );

            // Set up your ActionBar
            final ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled( false );
            actionBar.setDisplayShowTitleEnabled( false );
            actionBar.setDisplayShowCustomEnabled( true );
            actionBar.setCustomView( actionBarLayout );

            TextView title = (TextView) actionBarLayout.findViewById( R.id.action_bar_title );
            title.setTypeface( FontUtil.getTypeface( this, FontUtil.FontType.TITLE_FONT ) );
            title.setText( mTitle );
        }
        else
        {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setNavigationMode( ActionBar.NAVIGATION_MODE_STANDARD );
            actionBar.setDisplayShowTitleEnabled( true );
            actionBar.setTitle(mTitle);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (!mNavigationDrawerFragment.isDrawerOpen())
        {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button_bkg_normal, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        Profile profile = DataLayer.get().getProfile();

        switch( id )
        {
            case R.id.action_settings:
            {
                return true;
            }
            default:
            {

                return super.onOptionsItemSelected(item);
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
    {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
        {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment()
        {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity)
        {
            super.onAttach(activity);
        }


        public void onFragmentInteraction( Uri uri )
        {
            Log.i(TAG, "HERE" );
        }
    }

}
