package com.manvscode.financialadvisor;


import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.TypedValue;

import com.manvscode.wealth.AssetClass;
import com.manvscode.wealth.LiabilityClass;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

public class Utils
{
    public static String formatCurrency( double value, int digits )
    {
        NumberFormat nf = NumberFormat.getCurrencyInstance( );
        nf.setMaximumFractionDigits( digits );

        Currency currency = Currency.getInstance( Locale.getDefault() );
        nf.setCurrency( currency );

        return nf.format( value );
    }

    private static String formatCurrency( double value, int digits, String currencyCode )
    {
        NumberFormat nf = NumberFormat.getCurrencyInstance( );
        nf.setMaximumFractionDigits( digits );

        Currency currency = Currency.getInstance( currencyCode );
        nf.setCurrency( currency );

        return nf.format( value );
    }

    public static String formatPercentage( double value, int digits )
    {
        NumberFormat nf = NumberFormat.getPercentInstance( );
        nf.setMaximumFractionDigits( digits );
        return nf.format( value );
    }


    public static String dateShortString( long timestamp )
    {
        try
        {
            DateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy" );
            Date netDate = new Date(timestamp * 1000);
            return sdf.format(netDate);
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static int convertDipsToPixels( int dp )
    {
        Resources r = FinancialAdvisorApp.get().getResources();
        return (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics() );
    }



    public static GraphicalView createPieChart( Context ctx, String[] names, double[] values, int[] colors, DefaultRenderer renderer )
    {
        CategorySeries series = new CategorySeries("");

        for (int i = 0; i < values.length; i++)
        {
            series.add( names[ i ], values[ i ] );
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer( );
            seriesRenderer.setColor( colors[ (series.getItemCount() - 1) % colors.length ] );
            renderer.addSeriesRenderer( seriesRenderer );
        }

        GraphicalView chartView = ChartFactory.getPieChartView( ctx, series, renderer );
        return chartView;
    }

    public static float convertDipsToPixels( Context ctx, int dp )
    {
        Resources r = ctx.getResources( );
        return TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 14, r.getDisplayMetrics( ) );
    }

    public static String assetClassString( Context context, AssetClass ac )
    {
        TypedArray assetClassStringArray = context.getResources().obtainTypedArray( R.array.asset_classes );
        return assetClassStringArray.getString( ac.getValue() );
    }

    public static String liabilityClassString( Context context, LiabilityClass lc )
    {
        TypedArray assetClassStringArray = context.getResources().obtainTypedArray( R.array.liability_classes );
        return assetClassStringArray.getString( lc.getValue() );
    }

    public static String numberToString( double x )
    {
        String result;

        try
        {
            result = Double.toString( x );
        }
        catch( Exception e )
        {
            result = "";
        }

        return result;
    }

    public static String numberToString( int x )
    {
        String result;

        try
        {
            result = Integer.toString( x );
        }
        catch( Exception e )
        {
            result = "";
        }

        return result;
    }


}
