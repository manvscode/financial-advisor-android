package com.manvscode.financialadvisor;

import android.app.Application;

import com.manvscode.financialadvisor.data.DataLayer;

public class FinancialAdvisorApp extends Application
{
    private static FinancialAdvisorApp mInstance = null;

    public static FinancialAdvisorApp get( )
    {
        return mInstance;
    }

    @Override
    public void onCreate( )
    {
        super.onCreate();
        mInstance = this;

        DataLayer dataLayer = DataLayer.get( );
        dataLayer.load();
    }
}
